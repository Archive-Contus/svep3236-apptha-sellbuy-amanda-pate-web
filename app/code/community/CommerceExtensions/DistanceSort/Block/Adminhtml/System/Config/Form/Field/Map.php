<?php

class CommerceExtensions_DistanceSort_Block_Adminhtml_System_Config_Form_Field_Map extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
  public function __construct()
  {  	   
	  $this->addColumn('attribute', array(
		  'label' => $this->__('Attribute'),
	  ));

	  $this->addColumn('mapped_field', array(
		  'label' => $this->__('Address Field'),
	  ));
	  $this->_addAfter = false;
	  $this->_addButtonLabel = $this->__('Add field');
	  parent::__construct();
	  $this->setTemplate('distancesort/config/form/field/array.phtml');
  }
  
  protected function _renderCellTemplate($columnName)
  {
	  if (empty($this->_columns[$columnName])) {
		  throw new Exception('Wrong column name specified.');
	  }
	  $column     = $this->_columns[$columnName];
	  $inputName  = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';
	  
	  $rendered = '<select name="'.$inputName.'">';
	  if($columnName == 'attribute'){
		  $attributes = Mage::getResourceModel('catalog/product_attribute_collection');
		  $attributes->addFieldToFilter('frontend_input',array('select','text'));
		  $attributes->addFieldToFilter('is_user_defined','1');		
	 
		  foreach ($attributes->getItems() as $attribute){
			$rendered .=  str_replace("'","\'",'<option value="'.$attribute->getAttributecode().'">'.$attribute->getFrontendLabel().'</option>');
		  }
	  }elseif($columnName == 'mapped_field'){
		  $rendered .= '<option value="street1">Address Line 1</option>';
		  $rendered .= '<option value="street2">Address Line 2</option>';
		  $rendered .= '<option value="city">City</option>';
		  $rendered .= '<option value="state">State/Province</option>';		  
		  $rendered .= '<option value="postalcode">Zip/Postal Code</option>';	
		  $rendered .= '<option value="country">Country</option>';
		  $rendered .= '<option value="fulladdress">Full Address Is In One Field</option>';	  		  
	  }
	  $rendered .= '</select>';

	  return $rendered;
  }  
}