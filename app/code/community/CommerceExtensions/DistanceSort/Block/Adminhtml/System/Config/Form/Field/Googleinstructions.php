<?php

class CommerceExtensions_DistanceSort_Block_Adminhtml_System_Config_Form_Field_Googleinstructions extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
	  return '
		<p><strong>To get a free Google Maps API Key:</strong></p>
		<ol style="list-style:decimal; margin-left:20px;">
		  <li>Visit the APIs console at <a href="https://code.google.com/apis/console/?noredirect" target="_blank">https://code.google.com/apis/console</a> and log in with your Google Account. If you do not have an account, you will need to create one.</li>
		  <li>Click the <strong>Services</strong> link in the left-hand menu in the
			APIs Console.</li>
		  <li>You may be required to first &quot;Create a Project&quot; before being shown the list of available APIs. If this happens, just name it as something you will remember such as &quot;Magento Distance Sort&quot;.</li>
		  <li>In the list of available Google APIs, you will need to activate two of them. Activate the <strong>Geocoding API</strong> and <strong>Google Maps JavaScript API v3</strong> services.</li>
		  <li>Once the service has been activated, your API key is available from the <strong>API Access</strong> page, in the <strong>Simple API Access</strong> section. Geocoding API applications use the <strong>Key for server apps</strong>. </li>
		</ol>	  
	  ';
    }	
}