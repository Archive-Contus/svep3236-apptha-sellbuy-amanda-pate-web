<?php

class CommerceExtensions_DistanceSort_Block_Adminhtml_System_Config_Form_Field_Binginstructions extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
	  return '<p>To get a free Bing Maps API Key click <a href="http://www.microsoft.com/maps/create-a-bing-maps-key.aspx" target="_blank">here</a>. Select the "Basic Key" and not the "Trial Key". If you do not have an account with Bing, you will need to create one.</p>';
    }	
}