<?php



class CommerceExtensions_DistanceSort_Block_Product_List extends Mage_Catalog_Block_Product_List

{

  protected function _getProductCollection()

  {

	// it is highly important that this class rewrite and event dispatch be here. its the only place we can successfully

	// alter the product collection

	$collection = parent::_getProductCollection();

	Mage::dispatchEvent('catalog_block_product_list_get_product_collection', array(

		'collection' => $collection,

		'block' => $this

	));	

	return $collection;

  }

  

  protected function _beforeToHtml()

  {

	$this->setCanDisplayDistance(false);

	if(Mage::getSingleton('distancesort/config')->isEnabled()){

	  $this->setCanDisplayDistance(true);

	}

	parent::_beforeToHtml();

  }

  

  protected function _afterToHtml($html)

  {

	$map = $this->getLayout()

			 ->createBlock('distancesort/location_map')

			 ->setTemplate('distancesort/map.phtml')

			 ->toHtml();

	return $html.$map;

  }

  

  public function getDistanceDisplayText($_product,$short=false)
  {
	if(!$this->hasData('units')){
	  $this->setData('units',Mage::getStoreConfig('distancesort/general/units'));
	}
	$units = $this->getData('units');
	$distance = $_product->getData(CommerceExtensions_DistanceSort_Model_Config::DISTANCE);
	
	if((int)$distance > 0){

	  $text = sprintf('%s %s',$distance,$units);
	  $location = $this->getLayout()->createBlock('distancesort/location_toolbar')->getUserLocationText();
	  if(!$this->hasData('user_location_text')){
		$this->setData('user_location_text',$location);	
	  }
	  if($this->getData('user_location_text') && $short === false){			
		$text .= " from {$this->getData('user_location_text')}";
	  }
	  return $text;
	}
	return null;
  }

}