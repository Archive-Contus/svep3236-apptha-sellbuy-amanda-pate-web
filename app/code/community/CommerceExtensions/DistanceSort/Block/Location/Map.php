<?php

class CommerceExtensions_DistanceSort_Block_Location_Map extends Mage_Core_Block_Template
{
  protected function _beforeToHtml()
  {
	$this->setCanShowMap(false);
	if(Mage::getSingleton('distancesort/config')->isEnabled() && Mage::getStoreConfig('distancesort/api/google_api_key') && Mage::getStoreConfig('distancesort/api/show_google_map')){

	  $block = Mage::app()->getLayout()->getBlock('product_list');
	  if(!$block){
		$block = Mage::app()->getLayout()->getBlock('search_result_list');
		if(!$block){
	      return;
		}
	  }	  
	  // if product list block exists, add distance
	  
	  $_session = Mage::getSingleton('core/session');	
	  if(!$_session->getUserLocation()){
		return;
	  }	
	  
	  $this->setGoogleApiKey(Mage::getStoreConfig('distancesort/api/google_api_key'));
	  
	  $toolbar = Mage::app()->getLayout()->getBlock('product_list_toolbar');
	  $collection = $toolbar->getCollection();
	  $this->setCollection($collection);

	  foreach($collection as $_product){
	    if($_product->getLatitude() && $_product->getLongitude()){
		  $this->setCanShowMap(true);
	      break;
		}
	  }
	}
	return parent::_beforeToHtml();
  }
}