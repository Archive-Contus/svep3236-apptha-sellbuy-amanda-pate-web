<?php

class CommerceExtensions_DistanceSort_Block_Location_Toolbar extends Mage_Core_Block_Template
{	
  protected function _prepareLayout()
  {		
    if(!$this->hasData('can_display_location_toolbar')){
	  $this->setData('can_display_location_toolbar',Mage::getSingleton('distancesort/config')->isEnabled());
	}
	
	if($this->getData('can_display_location_toolbar')){
	  $_session = Mage::getSingleton('core/session'); // set in observer
	  
	  $userLocation = null;
	  $userLocationText = null;
	  $canDisplayForm = true;
	  
	  if($_session->getUserLocation()){
		$userLocation = new Varien_Object($_session->getUserLocation());
		$userLocationText = $this->_getUserLocationText($userLocation);
		$canDisplayForm = !$userLocationText ? true : false;			 
	  }
	  $this->setUserLocation($userLocation);
	  $this->setUserLocationText($userLocationText);
	  $this->setCanDisplayForm($canDisplayForm);	
	}
	return parent::_prepareLayout();
  }     	
  
  public function _getUserLocationText($location=null)
  {
	if(is_null($location)){
	  $location = $this->getUserLocation();	
	}
	
	$text = null;
	
	if($location){
	  
	  // display city & state if data available
	  if($location->getCity() || $location->getState() || $location->getRegion()){
		if($location->getCity()){
		  $text .= $location->getCity();
		  if($location->getState() || $location->getRegion()){
			$text .= ', ';
		  }
		}
		if($location->getState() || $location->getRegion()){
		  $text .= $location->getState() ? $location->getState() : $location->getRegion();
		}	 
	  } elseif($location->getCountry() && $location->getPostalcode()){	  
	    // if city & state are not available and user submitted location via the form, display that data
		$text .= $location->getCountry().', '.$location->getPostalcode();		
	  }
	   	  
	}
	return trim($text);
  }
  
  public function getCountries()
  {
	return Mage::getModel('adminhtml/system_config_source_country')->toOptionArray();	  
  }	  
}