<?php

class CommerceExtensions_DistanceSort_AdminController extends Mage_Adminhtml_Controller_action
{  
  public function geocodeAction()
  {
	if(Mage::getSingleton('distancesort/config')->isEnabled()){
		
	  $key = $this->getRequest()->getParam('massaction_prepare_key');
	  $productIds = $this->getRequest()->getParam($key);
		  
	  if($productIds){
		
		try{
			
		  $_geocodeModel = Mage::getSingleton('distancesort/geocode');	
		  $i=0;
		  foreach($productIds as $productId){
			  
			$product = Mage::getModel('catalog/product')->load($productId);
			$coordinates = $_geocodeModel->getProductCoordinates($product);			
			$coordinates = array_filter($coordinates,'strlen');
			
			if(!empty($coordinates)){		 		
			  $product->unlockAttribute(CommerceExtensions_DistanceSort_Model_Config::LATITUDE);
			  $product->unlockAttribute(CommerceExtensions_DistanceSort_Model_Config::LONGITUDE);	  		  
			  $product->addData($coordinates);
			  if($product->save()) $i++;			
			}		
		  }
		  $this->_getSession()->addSuccess($this->__('%s products have been geocoded for use with Distance Sort functionality.',$i));
		  
		} catch(Exception $e){
		  $this->_getSession()->addError($e->getMessage());
		}
		
	  }
	  $this->_redirect('adminhtml/catalog_product');
	  
	} else {
	  $this->_redirectReferer();
	}
	return;
  }
}