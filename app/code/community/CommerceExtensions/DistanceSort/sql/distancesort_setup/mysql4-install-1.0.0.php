<?php

$installer = $this;
$installer->startSetup();

  $eav = Mage::getModel('eav/entity_setup','core_setup');
  $existingAttributes = Mage::getResourceModel('catalog/product_attribute_collection')
						  ->getColumnValues('attribute_code');
  // get all attribute sets
  $attributeSets = Mage::getResourceModel('eav/entity_attribute_set_collection')
					  ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId());							  
						  
  $attributeCodes = array(
  	CommerceExtensions_DistanceSort_Model_Config::LATITUDE  => 'Latitude',
	CommerceExtensions_DistanceSort_Model_Config::LONGITUDE => 'Longitude'
  );
  
  foreach($attributeCodes as $attributeCode => $attributeLabel){
	if(!in_array($attributeCode,$existingAttributes)) {
	  $attributeData = array(
		'type'              => 'text',
		'label'             => $attributeLabel,
		'input'             => 'text',
		'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => NULL,
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => true,
		'unique'            => false,
		'apply_to'          => 'all',
		'is_configurable'   => false,
		'note' 		        => 'This field is filled in automatically and/or updated whenever you add or change the location of the product.'
	  );
	  $eav->addAttribute(Mage_Catalog_Model_Product::ENTITY,$attributeCode,$attributeData);	    
	}
  }  
  
  $i=1;  
  foreach($attributeSets as $attributeSet) {
   $eav->addAttributeGroup(Mage_Catalog_Model_Product::ENTITY,$attributeSet->getAttributeSetName(),'Distance Sort', 10000);	
   $attributeGroupId = $eav->getAttributeGroupId(Mage_Catalog_Model_Product::ENTITY,$attributeSet->getAttributeSetName(),'Distance Sort');	  
   foreach($attributeCodes as $attributeCode => $attributeLabel){
	 $eav->addAttributeToSet(Mage_Catalog_Model_Product::ENTITY, $attributeSet->getAttributeSetId(),$attributeGroupId,$attributeCode,$i++);
   }
  }	  

$installer->endSetup();
