<?php

class CommerceExtensions_DistanceSort_Model_Geocode_Google extends CommerceExtensions_DistanceSort_Model_Geocode
{
  const SERVICE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?';
  
  public function getCoordinates($data)
  {
	$results = array();
	
	$apiKey = Mage::getStoreConfig('distancesort/api/google_api_key');
	if(!$apiKey){
	  return $results;
	}		
	
	$queryString = $this->_getQueryString($data,$apiKey);
	
	$url = self::SERVICE_URL.$queryString;

	$handle = curl_init();
	curl_setopt_array(
	  $handle,
	  array(
		CURLOPT_URL            => $url,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_RETURNTRANSFER => true,
	  )
	);	 
	
	$response = curl_exec($handle);
	curl_close($handle);
	$response = json_decode($response,true);
	if(strtolower($response['status']) != 'ok'){
	  return $results;
	}
	
	if(array_key_exists('results',$response)){
	  if(empty($response['results'])){
		return $results;
	  }

	  $resultsArray = $response['results'][0];
	  if(!array_key_exists('geometry',$resultsArray)){
		return $results;
	  }
	  
	  $geometryArray = $resultsArray['geometry'];
	  if(!array_key_exists('location',$geometryArray)){
		return $results;
	  }
	  
	  $locationArray = $geometryArray['location'];
	  if(!array_key_exists('lat',$locationArray) || !array_key_exists('lng',$locationArray)){
		return $results;
	  }
	  
	  $latitude  = $locationArray['lat'];
	  $longitude = $locationArray['lng'];
	  if(!strlen($latitude) || !strlen($longitude)){
		return $results;
	  }
	  
	  return array(
	  	CommerceExtensions_DistanceSort_Model_Config::LATITUDE  => $latitude,
		CommerceExtensions_DistanceSort_Model_Config::LONGITUDE => $longitude); 
	}
	return $results;		
  }
  
  protected function _getQueryString($data,$key)
  {
	$array['address'] = implode(' ',$data);
	$array['key'] = $key;
	$array['language'] = 'en';
	return http_build_query($array);				
  }
}