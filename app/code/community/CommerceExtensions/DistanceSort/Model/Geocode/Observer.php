<?php

class CommerceExtensions_DistanceSort_Model_Geocode_Observer extends Varien_Event_Observer
{
  public function geocodeProduct($observer)
  {
	if(Mage::getSingleton('distancesort/config')->isEnabled()){
	  $product = $observer->getProduct();
	  $_geocodeModel = Mage::getSingleton('distancesort/geocode');
	  $needToGeocode = false;
	  $fieldMap = $_geocodeModel->getFieldMap();
	  foreach($fieldMap as $field => $attributeCode){
		if($product->getData($attributeCode) != $product->getOrigData($attributeCode) || 
		  !$product->getOrigData() || 
		  (!$product->getData(CommerceExtensions_DistanceSort_Model_Config::LATITUDE) || !$product->getData(CommerceExtensions_DistanceSort_Model_Config::LONGITUDE))){
		  $needToGeocode = true;
		  break;
		}
	  }
  
	  // will only geocode if data changes so we dont unnecessarily make geocoding calls
	  if($needToGeocode){
		$coordinates = $_geocodeModel->getProductCoordinates($product);
		if(!empty($coordinates)){
		  $product->addData($coordinates);		
		}
	  }
	}
	return;
  }
  
  public function setUserLocation($observer)
  {
	if(Mage::getSingleton('distancesort/config')->isEnabled()){
	  
	  $_geocodeModel = Mage::getSingleton('distancesort/geocode');
	  $_session = Mage::getSingleton('core/session');	  	

	  // get coordinates by form post data
	  $post = Mage::app()->getRequest()->getPost();
	  if(isset($post['postalcode']) && isset($post['country'])){		  
		$coordinates = $_geocodeModel->getUserLocation($post);	
		if(!empty($coordinates)){
		  $userLocation = array_merge($coordinates,$post);
		  $userLocation['country'] = Mage::app()->getLocale()->getCountryTranslation($post['country']);
		  $userLocation = new Varien_Object($userLocation);
		}
	  } else {
		$userLocation = $_geocodeModel->getUserLocation();	
	  }
	  $_session->unsUserLocation(); // clear the data before setting new data
	  $_session->setUserLocation($userLocation);		  
	}	
	return;
  }    
}