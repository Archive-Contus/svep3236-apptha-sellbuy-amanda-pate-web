<?php

class CommerceExtensions_DistanceSort_Model_Geocode_User extends CommerceExtensions_DistanceSort_Model_Geocode
{
  protected $_api = 'http://www.telize.com/geoip/%s';
  protected $_properties = array(); 
  
  public function __get($key)
  {
	if(isset($this->_properties[$key])){
	  return $this->_properties[$key];
	}
	return null;
  }
  
  public function request($ip)
  {
	$url = sprintf($this->_api,$ip);
	$data = $this->_sendRequest($url);
	$this->_properties = json_decode($data,true);	
	$this->_properties = array_filter($this->_properties,'strlen');
	$this->setData($this->_properties);
  }
  
  protected function _sendRequest($url)
  {
	$curl = curl_init();
	curl_setopt_array(
	  $curl,
	  array(
		CURLOPT_URL            => $url,
		CURLOPT_RETURNTRANSFER => true,
	  )
	);
	return curl_exec($curl);		
  }
}