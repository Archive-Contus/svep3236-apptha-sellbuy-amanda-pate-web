<?php

class CommerceExtensions_DistanceSort_Model_Geocode_Bing extends CommerceExtensions_DistanceSort_Model_Geocode
{
  const SERVICE_URL = 'http://dev.virtualearth.net/REST/v1/Locations';
  
  public function getCoordinates($data)
  {	  
	// api instructions: http://msdn.microsoft.com/en-us/library/ff701714.aspx
	$results = array();
	
	$apiKey = Mage::getStoreConfig('distancesort/api/bing_api_key');
	if(!$apiKey){
	  return $results;
	}		
	
	$queryString = $this->_getQueryString($data,$apiKey);
	
	$url = self::SERVICE_URL.$queryString;
	
	$handle = curl_init();
	curl_setopt_array(
	  $handle,
	  array(
		CURLOPT_URL            => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HEADER         => false,
		CURLOPT_HTTPHEADER     => array('Accept' => 'application/xml')	
	  )
	);
	
	$response = curl_exec($handle);
	curl_close($handle);
	$response = json_decode($response,true);
	
	if(array_key_exists('resourceSets',$response)){
	  $resourceSets = $response['resourceSets'];
	  if(is_array($resourceSets)){
		if(array_key_exists(0,$resourceSets)){
		  $firstItem = $resourceSets[0];		  
		  if(array_key_exists('resources',$firstItem)){
			$resources = $firstItem['resources'];
			if(array_key_exists(0,$resources)){
			  if(array_key_exists('geocodePoints',$resources[0])){
				$geocodePoints = $resources[0]['geocodePoints'];				
				if(array_key_exists(0,$geocodePoints)){				 
				  if(array_key_exists('coordinates',$geocodePoints[0])){
					$coordinates = $geocodePoints[0]['coordinates'];
					if(is_array($coordinates)){
					  $results[CommerceExtensions_DistanceSort_Model_Config::LATITUDE] = $coordinates[0];
					  $results[CommerceExtensions_DistanceSort_Model_Config::LONGITUDE] = $coordinates[1];					
					}
				  }
				}
			  }
			}
		  }
		}
	  }
	}
	return $results;
  }
  
  protected function _getQueryString($data,$key)
  {
	$array = array();
		
	if(array_key_exists('fulladdress',$data)){
	
	  $array['query'] = $data['fulladdress'];
	
	} else {
	
	  $empty = null;
	  $street = null;
	  if(array_key_exists('street1',$data)){
		$street .= $data['street1'];
		if(array_key_exists('street2',$data)){
		  $street .= ' '.$data['street2'];
		}	  
	  }		
	  $array['addressLine'] = strlen($street) ? $street : $empty;
	  $array['locality'] = array_key_exists('city',$data) ? $data['city'] : $empty;
	  $array['adminDistrict'] = array_key_exists('state',$data) ? $data['state'] : $empty;
	  $array['postalCode'] = array_key_exists('postalcode',$data) ? $data['postalcode'] : $empty;
	  $array['countryRegion'] = array_key_exists('country',$data) ? $data['country'] : $empty;	  	  
	}
	$array['maxResults'] = 1;
	$array['key'] = $key;	
	$array = array_filter($array,'strlen');		
	return '?'.http_build_query($array);				
  }  
}