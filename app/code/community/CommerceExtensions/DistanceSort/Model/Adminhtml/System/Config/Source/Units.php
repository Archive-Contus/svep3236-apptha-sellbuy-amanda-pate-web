<?php

class CommerceExtensions_DistanceSort_Model_Adminhtml_System_Config_Source_Units
{
  public function toOptionArray()
  {
	return array(
	  array('value' => 'miles','label' => 'Miles'),
	  array('value' => 'kilometers','label' => 'Kilometers')
	);
  }
}