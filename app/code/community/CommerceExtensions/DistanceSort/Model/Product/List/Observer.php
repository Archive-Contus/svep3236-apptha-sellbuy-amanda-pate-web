<?php

class CommerceExtensions_DistanceSort_Model_Product_List_Observer extends Varien_Event_Observer
{ 
  public function addDistanceSortOption($observer)
  {	  
	$block = $observer->getEvent()->getBlock();
	if(Mage::getSingleton('distancesort/config')->isEnabled() && $block instanceof Mage_Catalog_Block_Product_List_Toolbar && Mage::registry('distance_added'))
	{	  	  
	  $block->addOrderToAvailableOrders(CommerceExtensions_DistanceSort_Model_Config::DISTANCE,'Distance');
	}
	return;
  }	
  
  public function sortByDistance($observer)
  {	
	if(Mage::getSingleton('distancesort/config')->isEnabled() && Mage::registry('distance_added'))
	{		
	  $block = $observer->getBlock();
	  $toolbar = $block->getToolbarBlock();

	  if($toolbar){
		$order = Mage::app()->getRequest()->getParam($toolbar->getOrderVarName());
		$dirParam = Mage::app()->getRequest()->getParam($toolbar->getDirectionVarName());
		$dir = $dirParam ? $dirParam : $toolbar->getCurrentDirection();
		
		// if we want distance to be the default sort order
		if(Mage::getStoreConfig('distancesort/general/default_order') && !$order){			
		  $order = CommerceExtensions_DistanceSort_Model_Config::DISTANCE;		  
		  $toolbar->setData('_current_grid_order',$order);
		  $toolbar->setData('_current_grid_direction',$dir);		  						
		}			
				  
		if($order == CommerceExtensions_DistanceSort_Model_Config::DISTANCE){
		  
		  $collection = $observer->getCollection();
		  $toolbar->setData('_current_grid_order',$order);
		  $toolbar->setData('_current_grid_direction',$dir);	
		  		  
		  $collection->getSelect()->reset(Zend_Db_Select::ORDER);
		  $operator = $dir == 'desc' ? '!=' : '=';
		  $collection->getSelect()->order(new Zend_Db_Expr("`{$order}` {$operator} 0 {$dir}"));
		  $collection->getSelect()->order(new Zend_Db_Expr("`{$order}` {$dir}"));
		  $toolbar->setCollection($collection);		  		  				
		}
	  }	  
	}
	return;	  
  }
  
  public function addUserLocationField($observer)
  {
	if(Mage::getSingleton('distancesort/config')->isEnabled()){
	  $block = $observer->getBlock();
	  if($block instanceof Mage_Catalog_Block_Product_List_Toolbar)
	  {		
        $transport = $observer->getTransport();
		$html = $transport->getHtml();
        $preHtml = Mage::app()->getLayout()
				     ->createBlock('distancesort/location_toolbar')
					 ->setTemplate('distancesort/toolbar.phtml')
					 ->toHtml();        
        $html = $preHtml . $html;
        $transport->setHtml($html);
	  }
	}
	return;	  
  }  
}