<?php

class CommerceExtensions_DistanceSort_Model_Product_Collection_Observer extends Varien_Event_Observer
{  
  public function addDistanceToCollection($observer)
  {	 
	if(Mage::getSingleton('distancesort/config')->isEnabled()){

	  $block = Mage::app()->getLayout()->getBlock('product_list');
	  if(!$block){
		$block = Mage::app()->getLayout()->getBlock('search_result_list');
		if(!$block){
	      return;
		}
	  }	  
	  
	  // if product list block exists, add distance	  
	  $_session = Mage::getSingleton('core/session');	
	  if(!$_session->getUserLocation()){
		return;
	  }	
	  
	  // if user location can be obtained, continue to add distance
	  
	  $distanceColumn = CommerceExtensions_DistanceSort_Model_Config::DISTANCE;
	  $latitudeColumn = CommerceExtensions_DistanceSort_Model_Config::LATITUDE;
	  $longitudeColumn = CommerceExtensions_DistanceSort_Model_Config::LONGITUDE;
	  $userLatitude = $_session->getUserLocation()->getData($latitudeColumn);
	  $userLongitude = $_session->getUserLocation()->getData($longitudeColumn);
						  
	  // if we have incomplete user location data, do nothing
	  if(!$userLatitude || !$userLongitude){
		return;
	  }		  
	  		  	
	  $collection = $observer->getCollection();	
	  
	  $nullDistance = 0;
	  	  
	  if(!Mage::registry('distance_added')){
		// this ensures we only add distance once and avoid errors	 
		
		// join latitude and longitude to collection here
		foreach(array($latitudeColumn,$longitudeColumn) as $key => $attribute){
		  $table = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attribute)->getBackend()->getTable();
		  $attributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product',$attribute)->getAttributeId();	  
		  $alias = "at{$key}";
		  $collection->getSelect()->join(
			array($alias => $table), 
			"e.entity_id = {$alias}.entity_id", 
			array($attribute => "{$alias}.value"))
			  ->where("{$alias}.attribute_id = ?", $attributeId);	 
		}		
		   
		$units = (Mage::getStoreConfig('distancesort/general/units') == 'miles') ? 3959 : 6371;  
		$unitsText = Mage::getStoreConfig('distancesort/general/units');
		$distanceSql = new Zend_Db_Expr("
		  (SELECT(CAST(IF({$latitudeColumn} IS NULL OR {$latitudeColumn} LIKE '' OR {$longitudeColumn} IS NULL OR {$longitudeColumn} LIKE '','{$nullDistance}', 
			({$units} * ACOS(COS(RADIANS({$userLatitude})) * COS(RADIANS({$latitudeColumn})) * COS(RADIANS({$longitudeColumn}) - RADIANS({$userLongitude})) + SIN(RADIANS({$userLatitude})) * SIN(RADIANS({$latitudeColumn}))))) AS DECIMAL(10,1))))");		
//		$collection->addExpressionAttributeToSelect($distanceColumn,$distanceSql,array($latitudeColumn,$longitudeColumn));
		$collection->getSelect()
		  ->columns(
			array(
			  $distanceColumn => new Zend_Db_Expr($distanceSql),
			  'distance_units' => new Zend_Db_Expr("'{$unitsText}'")
			)
		  );
		Mage::register('distance_added',true);		
		
		// we have to run custom sql here because if we loop through the actual collection, it gets loaded and we dont want that
		$sql = $collection->getSelect();
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$statement = $readConnection->query($sql);
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);

		// we run this quick loop and if no products at all have distance, we disable the module
		$enableModel = false;
		$nullDistance = number_format((float)$nullDistance,1, '.', '');
		$nullAsc = number_format((float)CommerceExtensions_DistanceSort_Model_Config::NULL_ASC,1, '.', '');
		$nullDesc = number_format((float)CommerceExtensions_DistanceSort_Model_Config::NULL_DESC,1, '.', '');
		$nulls = array($nullDistance,$nullAsc,$nullDesc);
		foreach($result as $product){
		  $productDistance = number_format((float)$product[$distanceColumn],1, '.', '');	  
		  if(!in_array($productDistance,$nulls)){
			$enableModel = true;
			break;
		  }
		}
		if(!$enableModel) Mage::getSingleton('distancesort/config')->setIsEnabled(false);
	  }	  
	}
	return;	 
  }	 
}