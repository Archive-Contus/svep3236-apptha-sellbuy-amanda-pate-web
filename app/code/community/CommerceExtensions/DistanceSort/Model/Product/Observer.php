<?php

class CommerceExtensions_DistanceSort_Model_Product_Observer extends Varien_Event_Observer
{
  public function lockAttributes($observer)
  {
    $event = $observer->getEvent();
    $product = $event->getProduct();
    $product->lockAttribute(CommerceExtensions_DistanceSort_Model_Config::LATITUDE);
	$product->lockAttribute(CommerceExtensions_DistanceSort_Model_Config::LONGITUDE);	  
  }
  
  public function addMassAction($observer)
  {
	if(Mage::getSingleton('distancesort/config')->isEnabled()){
	  $block = $observer->getEvent()->getBlock();
	  if($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction)
	  {
		$block->addItem('geocode', array(
		  'label'   => 'Geocode Products',
		  'url'     => Mage::helper("adminhtml")->getUrl('distancesort/admin/geocode'),
		  'confirm' => Mage::helper('distancesort')->__('Are you sure you want to geocode each of these products? Please note that any products that do not have location information of some sort will be skipped.')
		));
	  }
	}
  }
  
  public function addDistanceToProduct($observer)
  {
	if(Mage::getSingleton('distancesort/config')->isEnabled()){

	  $_product = $observer->getProduct();
	  $_user = Mage::getSingleton('core/session')->getUserLocation();

	  $distance = null;
	  $unitsText = null;
	  
	  if($_user){
		  if($_product->getLatitude() && $_product->getLongitude() && $_user->getLatitude() && $_user->getLongitude()){
			  
			$units = (Mage::getStoreConfig('distancesort/general/units') == 'miles') ? 3959 : 6371; 
			$unitsText = Mage::getStoreConfig('distancesort/general/units');	
				  
		    $distance = ( $units * acos((cos(deg2rad($_user->getLatitude())) ) * (cos(deg2rad($_product->getLatitude()))) * (cos(deg2rad($_product->getLongitude()) - deg2rad($_user->getLongitude())) )+ ((sin(deg2rad($_user->getLatitude()))) * (sin(deg2rad($_product->getLatitude()))))) );
			$distance = round($distance,1);
		  }
		  $_product->setDistance($distance);
		  $_product->setDistanceUnits($unitsText);
		  return;	  
	  }
	}
  }
}