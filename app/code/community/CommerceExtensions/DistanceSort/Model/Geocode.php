<?php

class CommerceExtensions_DistanceSort_Model_Geocode extends Mage_Core_Model_Abstract
{
  protected function _getSession()
  {
	return Mage::getSingleton('core/session');
  }
  
  public function getFieldMap()
  {
	$fieldMap = array();
	$array = unserialize(Mage::getStoreConfig('distancesort/attribute_map/address_fields'));
	foreach($array as $id => $values){
	  $fieldMap[$values['mapped_field']] = $values['attribute'];
	}
	return $fieldMap;
  }
  
  public function getMappedProductData($product)
  {
	$mappedProductData = array();
	
	if(!$product->getData()){
	  return $mappedProductData;
	}
	$fieldMap = $this->getFieldMap();
	
	foreach($fieldMap as $mappedField => $attributeCode){
	  $mappedProductData[$mappedField] = ($data = $product->getData($attributeCode)) ? $data : null;
	}
	$mappedProductData = array_filter($mappedProductData,'strlen');
	$mappedProductData = $this->_orderArray($mappedProductData);
	return $mappedProductData;
  }
  
  protected function _geocodeGoogle($data)
  {
	$model = Mage::getModel('distancesort/geocode_google');
	$results = $model->getCoordinates($data);
	return $results;
  }
  
  protected function _geocodeBing($data)
  {
	$model = Mage::getModel('distancesort/geocode_bing');
	$results = $model->getCoordinates($data);
	return $results;
  }  
  
  protected function _orderArray(array $data)
  {
    $ordered = array();
	$orderArray = array(
	  'street1',
	  'street2',
	  'city',
	  'state',
	  'postalcode',
	  'country',
	  'fulladdress'	  
	);
    foreach($orderArray as $key) {
        if(array_key_exists($key,$data)) {
            $ordered[$key] = $data[$key];
            unset($data[$key]);
        }
    }
    return $ordered + $data;	  
  }
  
  
  public function getCoordinates($data)
  {
	$coordinates = $this->_geocodeGoogle($data);
	if(empty($coordinates)){
	  $coordinates = $this->_geocodeBing($data);
	}	
	return $coordinates;	  
  }
  
  public function getProductCoordinates($product)
  {
	$data = $this->getMappedProductData($product);
	$coordinates = $this->getCoordinates($data);
	if(empty($coordinates)){
	  return array(
	    CommerceExtensions_DistanceSort_Model_Config::LATITUDE => NULL,
		CommerceExtensions_DistanceSort_Model_Config::LONGITUDE => NULL
	  );
	}
	return $coordinates;
  }
  
  public function getUserCoordinatesByIp()
  {
	$ip = Mage::helper('core/http')->getRemoteAddr();	
	if(!$ip || $ip == '127.0.0.1'){
		return array();
	}	
	
	$_session = $this->_getSession();
	if(!$_session->getData('user_location_by_ip')){		
	  $geo = Mage::getSingleton('distancesort/geocode_user'); 
	  $geo->request($ip);
	  $data = $geo->getData();		  
	  $_session->setData('user_location_by_ip',new Varien_Object($data));	 
	}			
	return $_session->getData('user_location_by_ip');  
  }
  
  public function getUserLocation($data=null)
  {	
	$_session = $this->_getSession();
	
	if($data){
			  
	  // if data passed in, use it to get user coordinates
	  $coordinates = $this->getCoordinates($data);
	  $_session->setUserLocation($coordinates);
	  
	} else {
	  
	  // otherwise, get user coordinates from their ip address
	  if(!$_session->getUserLocation()){
	    $_session->setUserLocation($this->getUserCoordinatesByIp());
	  }
	}
	return $_session->getUserLocation();
  }  
}