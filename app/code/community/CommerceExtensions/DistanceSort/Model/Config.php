<?php

class CommerceExtensions_DistanceSort_Model_Config extends Mage_Core_Model_Abstract
{
  const DISTANCE  = 'distance';
  const LATITUDE  = 'latitude';
  const LONGITUDE = 'longitude';
  const NULL_ASC = 1000000;
  const NULL_DESC = -1;
  
  protected $_isEnabled = null;
  
  public function isEnabled()
  {
	
	if($this->_isEnabled === false && is_bool($this->_isEnabled)){
	  // we can manually disable module if needed, from another function
	  return false;
	}
	
	$moduleEnabled = Mage::getStoreConfig('distancesort/general/enabled');
	$googleApiKey = Mage::getStoreConfig('distancesort/api/google_api_key');
	$bingApiKey = Mage::getStoreConfig('distancesort/api/bing_api_key');
	return $moduleEnabled && ($googleApiKey || $bingApiKey) ? true : false;
  }
  
  public function setIsEnabled($trueFalse=null)
  {
	$this->_isEnabled = $trueFalse;
  }
}