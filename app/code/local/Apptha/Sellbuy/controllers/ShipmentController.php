<?php

/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Sellbuy
 * @version     1.6
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 * 
 */

class Apptha_Sellbuy_ShipmentController extends Mage_Core_Controller_Front_Action {
    
    public function indexAction() {
    
    }
    
    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession() {
        return Mage::getSingleton ( 'core/session' );
    }
    /**
     * Initialize shipment items QTY
     */
    protected function _getItemQtys() {
        $data = $this->getRequest ()->getParam ( 'shipment' );
        if (isset ( $data ['items'] )) {
            $qtys = $data ['items'];
        } else {
            $qtys = array ();
        }
        return $qtys;
    }
    
    /**
     * Initialize shipment model instance
     *
     * @return Mage_Sales_Model_Order_Shipment bool
     */
    protected function _initShipment() {
        /**
         * Initialising shipment variable with false
         */
        $shipment = false;
        $shipmentId = $this->getRequest ()->getParam ( 'shipment_id' );
        $orderId = $this->getRequest ()->getParam ( 'order_id' );
        /**
         * check shipment id existance
         */
        if ($shipmentId) {
            $shipment = Mage::getModel ( 'sales/order_shipment' )->load ( $shipmentId );
        }
        /**
         * check order id existance
         */
        if ($orderId) {
            $order = Mage::getModel ( 'sales/order' )->load ( $orderId );
            
            /**
             * Check order existing
             */
            if (! $order->getId ()) {
                $this->_getSession ()->addError ( $this->__ ( 'The order no longer exists.' ) );
                return false;
            }
            
            $savedQtys = $this->_getItemQtys ();
            $shipment = Mage::getModel ( 'sales/service_order', $order )->prepareShipment ( $savedQtys );
            
            $tracks = $this->getRequest ()->getPost ( 'tracking' );
            foreach ( $tracks as $data ) {
                /**
                 * check the empty value of trancking number
                 */
                if (empty ( $data ['number'] )) {
                    Mage::throwException ( $this->__ ( 'Tracking number cannot be empty.' ) );
                }
                $track = Mage::getModel ( 'sales/order_shipment_track' )->addData ( $data );
                $shipment->addTrack ( $track );
            }
        }
        
        Mage::register ( 'current_shipment', $shipment );
        return $shipment;
    }
    
    /**
     * Save shipment
     * We can save only new shipment.
     * Existing shipments are not editable
     *
     * @return null
     */
    public function savePostAction() {
        try {
            $data = $this->getRequest ()->getParam ( 'shipment' );
            $shipment = $this->_initShipment ();
            /**
             * check the non existance of shipment
             */
            if (! $shipment) {
                $this->_forward ( 'noRoute' );
                return;
            }
            
            $shipment->register ();
            /**
             * initialising comment with empty value
             */
            $comment = '';
            
            /**
             * confirm comment text has some value without empty
             */
            if (! empty ( $data ['comment_text'] )) {
                $shipment->addComment ( $data ['comment_text'], isset ( $data ['comment_customer_notify'] ), isset ( $data ['is_visible_on_front'] ) );
                /**
                 * check comment_customer_notify has been already set
                 */
                $comment = $this->getcommentData ( $data );
            }
            
            $shipment->setEmailSent ( true );
            
            $shipment->getOrder ()->setCustomerNoteNotify ( true );
            $responseAjax = new Varien_Object ();
            $isNeedCreateLabel = isset ( $data ['create_shipping_label'] ) && $data ['create_shipping_label'];
            
            $responseAjax = $this->setResponseAjax ( $isNeedCreateLabel, $shipment, $responseAjax );
            /**
             * Save shipment information
             */
            $this->_saveShipment ( $shipment );
            /**
             * send shipment mail with comment
             */
            $shipment->sendEmail ( true, $comment );
            
            $shipmentCreatedMessage = $this->__ ( 'The shipment has been created.' );
            $labelCreatedMessage = $this->__ ( 'The shipping label has been created.' );
            
            $this->_getSession ()->addSuccess ( $isNeedCreateLabel ? $shipmentCreatedMessage . ' ' . $labelCreatedMessage : $shipmentCreatedMessage );
        
        } catch ( Mage_Core_Exception $e ) {
            if ($isNeedCreateLabel) {
                $responseAjax->setError ( true );
                $responseAjax->setMessage ( $e->getMessage () );
            } else {
                /**
                 * save error message in session
                 */
                $this->_getSession ()->addError ( $e->getMessage () );
                /**
                 * Redirect to manage order section
                 */
                $this->_redirect ( 'sellbuy/order/manage' );
            }
        } catch ( Exception $e ) {
            Mage::logException ( $e );
            if ($isNeedCreateLabel) {
                $responseAjax->setError ( true );
                $responseAjax->setMessage ( Mage::helper ( 'sales' )->__ ( 'An error occurred while creating shipping label.' ) );
            } else {
                /**
                 * add error message in session
                 */
                $this->_getSession ()->addError ( $this->__ ( 'Cannot save shipment.' ) );
                /**
                 * Redirect user to manage section in order section
                 */
                $this->_redirect ( 'sellbuy/order/manage' );
            }
        
        }
        
        $this->isNeedCreateLable ( $isNeedCreateLabel, $responseAjax );
    }
    
    /**
     * Save shipment and order in one transaction
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment            
     * @return Mage_Adminhtml_Sales_Order_ShipmentController
     */
    protected function _saveShipment($shipment) {
        $shipment->getOrder ()->setIsInProcess ( true );
        Mage::getModel ( 'core/resource_transaction' )->addObject ( $shipment )->addObject ( $shipment->getOrder () )->save ();
        
        return $this;
    }
    
    /**
     * Create shipping label for specific shipment with validation.
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment            
     * @return bool
     */
    protected function _createShippingLabel(Mage_Sales_Model_Order_Shipment $shipment) {
        
        $returnFlag = 0;
        /**
         * return if the shipment has not been set
         */
        if (! $shipment) {
            return false;
        }
        $carrier = $shipment->getOrder ()->getShippingCarrier ();
        
        $returnFlag = $this->getReturnFlag ( $carrier );
        
        $shipment->setPackages ( $this->getRequest ()->getParam ( 'packages' ) );
        $response = Mage::getModel ( 'shipping/shipping' )->requestToShipment ( $shipment );
        if ($response->hasErrors ()) {
            Mage::throwException ( $response->getErrors () );
        }
        
        $returnFlag = $this->responseHasInfo ( $response, $returnFlag );
        
        if ($returnFlag == 1) {
            return false;
        }
        
        $labelsContent = array ();
        $trackingNumbers = array ();
        $info = $response->getInfo ();
        foreach ( $info as $inf ) {
            /**
             * Checking tracking_number and label_content are not empty
             */
            $isTrackingNumberLable = $this->isTrackingNumberLable ( $inf );
            
            if ($isTrackingNumberLable == 1) {
                $labelsContent [] = $inf ['label_content'];
                $trackingNumbers [] = $inf ['tracking_number'];
            }
        }
        $outputPdf = $this->_combineLabelsPdf ( $labelsContent );
        $shipment->setShippingLabel ( $outputPdf->render () );
        $carrierCode = $carrier->getCarrierCode ();
        $carrierTitle = Mage::getStoreConfig ( 'carriers/' . $carrierCode . '/title', $shipment->getStoreId () );
        if ($trackingNumbers) {
            foreach ( $trackingNumbers as $trackingNumber ) {
                $track = Mage::getModel ( 'sales/order_shipment_track' )->setNumber ( $trackingNumber )->setCarrierCode ( $carrierCode )->setTitle ( $carrierTitle );
                $shipment->addTrack ( $track );
            }
        }
        return true;
    }
    /**
     * Set response ajax
     *
     * @param number $isNeedCreateLabel            
     * @param array $shipment            
     * @param array $responseAjax            
     * @return array $responseAjax
     */
    public function setResponseAjax($isNeedCreateLabel, $shipment, $responseAjax) {
        if ($isNeedCreateLabel && $this->_createShippingLabel ( $shipment )) {
            $responseAjax->setOk ( true );
        }
        return $responseAjax;
    }
    /**
     * Get comment data
     * 
     * @param array $data            
     * @return string $comment
     */
    public function getcommentData($data) {
        $comment = '';
        if (isset ( $data ['comment_customer_notify'] )) {
            $comment = $data ['comment_text'];
        }
        return $comment;
    }
    /**
     * Is need create lable
     *
     * @param string $isNeedCreateLabel            
     * @param array $responseAjax            
     */
    public function isNeedCreateLable($isNeedCreateLabel, $responseAjax) {
        if ($isNeedCreateLabel) {
            $this->getResponse ()->setBody ( $responseAjax->toJson () );
        } else {
            /**
             * Redirect to market place manage order section
             */
            $this->_redirect ( 'sellbuy/order/manage' );
        }
    }
    
    /**
     * Get return flag
     *
     * @param array $carrier            
     * @return number $returnFlag
     */
    public function getReturnFlag($carrier) {
        $returnFlag = 0;
        if (! $carrier->isShippingLabelsAvailable ()) {
            $returnFlag = 1;
        }
        return $returnFlag;
    }
    /**
     * Response has info for returnFlag
     * 
     * @param array $response            
     * @param number $returnFlag            
     * @return number $returnFlag
     */
    public function responseHasInfo($response, $returnFlag) {
        if (! $response->hasInfo ()) {
            $returnFlag = 1;
        }
        return $returnFlag;
    }
    
    /**
     * Checking tracking_number and label_content are not empty
     *
     * @param array $inf            
     * @return number $isTrackingNumberLable
     */
    public function isTrackingNumberLable($inf) {
        $isTrackingNumberLable = 0;
        if (! empty ( $inf ['tracking_number'] ) && ! empty ( $inf ['label_content'] )) {
            $isTrackingNumberLable = 1;
        }
        return $isTrackingNumberLable;
    }
} 