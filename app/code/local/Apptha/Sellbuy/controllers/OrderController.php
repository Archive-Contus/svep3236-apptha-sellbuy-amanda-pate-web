<?php
/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Sellbuy
 * @version     0.1.0
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 * 
 */
/**
 * Manage orders in front end section
 * This class has been used to manage the seller orders info like
 * view order
 * view transaction
 * cancel order
 */
class Apptha_Sellbuy_OrderController extends Mage_Core_Controller_Front_Action {
 /**
  * Retrieve customer session model object
  *
  * @return Mage_Customer_Model_Session
  */
 protected function _getSession() {
  return Mage::getSingleton ( 'customer/session' );
 }
 /**
  * Load phtml layout file to display order information
  *
  * @return void
  */
 public function indexAction() {
 /**
 * Check the customer is not logged in already
 * If so display message like you must have seller account
 * and redirect user to the seller login page
 */
  if (! $this->_getSession ()->isLoggedIn ()) {
   Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( 'You must have a Seller Account to access this page' ) );
   $this->_redirect ( 'sellbuy/seller/login' );
   return;
  }
  $this->loadLayout ();
  $this->renderLayout ();
 }
 
 /**
  * Manage orders by sellers
  *
  * @return void
  */
 public function manageAction() {
  Mage::helper ( 'sellbuy' )->checkSellbuyKey ();
  /**
   * Check the user is currently logged in
   * if so then show message like you must have a seller account
   * and redirect the user to the login page for seller
   */
  if (! $this->_getSession ()->isLoggedIn ()) {
   Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( 'You must have a Seller Account to access this page' ) );
   $this->_redirect ( 'sellbuy/seller/login' );
   return;
  }
  $this->loadLayout ();
  $this->renderLayout ();
 }
 /**
  * View full order information by seller
  *
  * @return void
  */
 public function vieworderAction() {
  Mage::helper ( 'sellbuy' )->checkSellbuyKey ();
  /**
   * Before viewing the order information we are checking the user is logged in
   * if he is not logged in show error message like he cant access this page without account
   * and redirect the current user to the seller login page
   */
  if (! $this->_getSession ()->isLoggedIn ()) {
   Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( 'You must have a Seller Account to access this page' ) );
   $this->_redirect ( 'sellbuy/seller/login' );
   return;
  }
  $this->loadLayout ();
  $this->renderLayout ();
 }
 /**
  * View full transaction history by seller
  *
  * @return void
  */
 function viewtransactionAction() {
  Mage::helper ( 'sellbuy' )->checkSellbuyKey ();
  /**
   * Checking if the user is already logged in
   * if not display no access message
   * and then redirect the customer to the login page for seller
   */
  if (! $this->_getSession ()->isLoggedIn ()) {
   Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( 'You must have a Seller Account to access this page' ) );
   $this->_redirect ( 'sellbuy/seller/login' );
   return;
  }
  $this->loadLayout ();
  $this->renderLayout ();
 }
 /**
  * Seller payment acknowledgement
  *
  * @return void
  */
 function acknowledgeAction() {
  Mage::helper ( 'sellbuy' )->checkSellbuyKey ();
  /**
   * Confirming that logged in session not available for the user
   * if it is then show the no acccess message to the user
   * and redirecting to the seller login page
   */
  if (! $this->_getSession ()->isLoggedIn ()) {
   Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( 'You must have a Seller Account to access this page' ) );
   $this->_redirect ( 'sellbuy/seller/login' );
   return;
  }
  $this->loadLayout ();
  $this->renderLayout ();
  /**
   * Get the commissin id
   */
  $commissionId = $this->getRequest ()->getParam ( 'commissionid' );
  /**
   * Check the commission id is not equal to empty
   * if so then get the collectin details
   * and redirect the user to the view transaction of the orders page
   * if commssion id is empty then redirect the user to view transactionpage with error message
   */
  if ($commissionId != '') {
   $collection = Mage::getModel ( 'sellbuy/transaction' )->changeStatus ( $commissionId );
   if ($collection == 1) {
    Mage::getSingleton ( 'core/session' )->addSuccess ( $this->__ ( "Payment received status has been updated" ) );
    $this->_redirect ( 'sellbuy/order/viewtransaction' );
   } else {
    Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( 'Payment received status was not updated' ) );
    $this->_redirect ( 'sellbuy/order/viewtransaction' );
   }
  }
 }
 /**
  * customer order cancel request
  *
  * @return void
  */
 public function cancelAction() {
     /**
      * Get the posted values and assign it to a variable
      */
  $data = $this->getRequest ()->getPost ();
  
  /**
   * Sending order email
   * Get the store configuratin like
   * template id
   * admin email id
   * to mail id
   * to name
   */
  $templateId = ( int ) Mage::getStoreConfig ( 'sellbuy/admin_approval_seller_registration/order_cancel_request_notification_template_selection' );
  $adminEmailId = Mage::getStoreConfig ( 'sellbuy/sellbuy/admin_email_id' );
  $toMailId = Mage::getStoreConfig ( "trans_email/ident_$adminEmailId/email" );
  $toName = Mage::getStoreConfig ( "trans_email/ident_$adminEmailId/name" );
  /**
   * Checking the template id has set
   * if so load the particular email template
   * if not load the default template for sellbuy cancel order
   */
  if ($templateId) {
   $emailTemplate = Mage::helper ( 'sellbuy/sellbuy' )->loadEmailTemplate ( $templateId );
  } else {
   $emailTemplate = Mage::getModel ( 'core/email_template' )->loadDefault ( 'sellbuy_cancel_order_admin_email_template_selection' );
  }
  $productCollection = Mage::getModel ( 'catalog/product' )->getCollection ()->addAttributeToSelect ( '*' )->addUrlRewrite ()->addAttributeToFilter ( 'entity_id', array (
    'in' => array (
      $data ['products'] 
    ) 
  ) );
  
  $productDetails = "<ul>";
  
  foreach ( $productCollection as $product ) {
   $productDetails .= "<li>";
   $productDetails .= "<div><a href='{$product->getProductUrl()}'>{$product->getName()}</a><div>";
   $productDetails .= "</li>";
  }
  
  $productDetails .= "</ul>";
  
  $incrementId = Mage::getModel ( 'sales/order' )->load ( $data ['order_id'] )->getIncrementId ();
  /**
   * Get the customer information using customer id like
   * seller email
   * seller name
   * recipient mail id
   * seller email template
   */
  $customer = Mage::getModel ( 'customer/customer' )->load ( $data ['customer_id'] );
  $sellerEmail = $customer->getEmail ();
  $sellerName = $customer->getName ();
  $recipient = $toMailId;
  $emailTemplate->setSenderName ( $sellerName );
  $emailTemplate->setSenderEmail ( $sellerEmail );
  /**
   * Dynamically replace the template variable with the retrieved values
   */
  $emailTemplateVariables = array (
    'ownername' => $toName,
    'productdetails' => $productDetails,
    'order_id' => $incrementId,
    'customer_email' => $sellerEmail,
    'customer_firstname' => $sellerName,
    'reason' => $data ['reason'] 
  );
  
  $emailTemplate->setDesignConfig ( array (
    'area' => 'frontend' 
  ) );
  /**
   * Sending email to admin
   */
  $emailTemplate->getProcessedTemplate ( $emailTemplateVariables );
  $emailSent = $emailTemplate->send ( $recipient, $toName, $emailTemplateVariables );
  if ($emailSent) {
      /**
       * If email has been sent to admin display success message
       */
   Mage::getSingleton ( 'core/session' )->addSuccess ( $this->__ ( "Order cancelation has been requested" ) );
   
   $this->_redirectSuccess ();
  
  } else {
      /**
       * If email has failed to admin display error message
       */
   Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( "Order cancelation has been not been send please try again" ) );
   
   $this->_redirectError ();
  }
 
 }
 /**
  * Function to request order cancel by seller
  *
  * @return void
  */
 public function requestOrderCancelAction(){
 
 	$orderId= $this->getRequest()->getParam('orderid');
 	$productId=$this->getRequest()->getParam('productid');
 	$order = Mage::getModel('catalog/product')->load($productId);
 	$model = Mage::getModel('sellbuy/updatestatus');
 	$orderStatus= $this->getRequest()->getParam('status');
 	$model = Mage::getModel('sellbuy/updatestatus');
 	$model->setOrderId($orderId);
 	$model->setStatus($orderStatus);
 	$model->setProductName($order['name']);
 	$model->save();
 	/**
 	 *  Initilize customer and seller group id
 	*/
 	$sellerGroupId = '';
 	$sellerGroupId = Mage::helper('sellbuy')->getGroupId();
 	/**
 	 *  Sending email to admin
 	*/
 	try {
 		$productId=$this->getRequest()->getParam('productid');
 		$orderId=$this->getRequest()->getParam('orderid');
 			
 		$templateId = (int) Mage::getStoreConfig('sellbuy/admin_approval_seller_registration/order_cancel_request');
 		$adminEmailId = Mage::getStoreConfig('sellbuy/sellbuy/admin_email_id');
 		$toMailId = Mage::getStoreConfig("trans_email/ident_$adminEmailId/email");
 		$toName = Mage::getStoreConfig("trans_email/ident_$adminEmailId/name");
 		/**
 		 *  Selecting template id
 		*/
 		if ($templateId) {
 			$emailTemplate = Mage::getModel('core/email_template')->load($templateId);
 		} else {
 			$emailTemplate = Mage::getModel('core/email_template')
 			->loadDefault('sellbuy_admin_approval_seller_registration_order_cancel_request');
 		}
 		$sellerId = Mage::getSingleton('customer/session')->getId();
 		$customer = Mage::getModel('customer/customer')->load($sellerId);
 		$seller_info = Mage::getModel('sellbuy/sellerprofile')->load($sellerId,'seller_id');
 		$selleremail = $customer->getEmail();
 		$recipient = $toMailId;
 		$sellername = $customer->getName();
 		$productName=$order['name'];
 		$emailTemplate->setSenderName($sellername);
 		$emailTemplate->setSenderEmail($selleremail);
 		$emailTemplateVariables = (array('ownername' => $toName,'selleremail'=>$selleremail,'sellername' => $sellername,'orderId'=>$orderId, 'selleremail' => $selleremail,'productName'=>$productName, 'message' => $message));
 		$emailTemplate->setDesignConfig(array('area' => 'frontend'));
 		$emailTemplate->getProcessedTemplate($emailTemplateVariables);
 		$emailTemplate->send($recipient, $sellername, $emailTemplateVariables);
 		$emailTemplate->send($selleremail, $sellername, $emailTemplateVariables);
 		Mage::getSingleton('core/session')->addSuccess($this->__('Cancel order request has been sent to admin'));
 		$this->_redirect('*/*/manage');
 	} catch (Mage_Core_Exception $e) {
 		/**
 		 *  Error message redirect to order manage page
 		 */
 		Mage::getSingleton('core/session')->addError($this->__($e->getMessage()));
 		$this->_redirect('*/*/manage');
 	}
 }
 /**
  * Function to set template for pop up
  *
  * @return void
  */
 public function popupAction(){
 	if (! $this->_getSession ()->isLoggedIn ()) {
 		Mage::getSingleton ( 'core/session' )->addError ( $this->__ ( 'You must have an account to access this page' ) );
 		$this->_redirect ( 'customer/account/login' );
 		return;
 	}
 	
 	$this->loadLayout ();
 	$this->renderLayout ();
 
 }
 /**
  * Function to send mail after seller updated order status
  *
  * @return void
  */
 public function updatestatusAction() {
 	 
 	$message = '';
 	$order_id= $this->getRequest()->getParam('orderid');
 	$sku=$this->getRequest()->getParam('sku');
 	$message= $this->getRequest()->getParam('update');
 	$model = Mage::getModel('sales/order');
 	$order = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
 	$model = Mage::getModel('sellbuy/updatestatus')->getCollection()->addFieldToFilter('order_id',$order_id)->addFieldToFilter('product_name',$order['name']);
 	foreach ($model as $data){
	 	$data->setOrderId($order_id);
	 	$data->setStatus('processing');
	 	$data->setProductName($order['name']);
	 	$data->save();
 	}
 	
 	/**
 	 *  Sending email to admin
 	*/
 	try {
 		$templateId = (int) Mage::getStoreConfig('sellbuy/admin_approval_seller_registration/update_order_status');
 		$adminEmailId = Mage::getStoreConfig('sellbuy/sellbuy/admin_email_id');
 		$toMailId = Mage::getStoreConfig("trans_email/ident_$adminEmailId/email");
 		$toName = Mage::getStoreConfig("trans_email/ident_$adminEmailId/name");
 
 		/**
 		 *  Selecting template id
 		*/
 		if ($templateId) {
 			$emailTemplate = Mage::getModel('core/email_template')->load($templateId);
 		} else {
 			$emailTemplate = Mage::getModel('core/email_template')
 			->loadDefault('sellbuy_admin_approval_seller_registration_update_order_status');
 		}
 		$sellerId = Mage::getSingleton('customer/session')->getId();
 		$customer = Mage::getModel('customer/customer')->load($sellerId);
 		$seller_info = Mage::getModel('sellbuy/sellerprofile')->load($sellerId,'seller_id');
 		$selleremail = $customer->getEmail();
 		$productName=$order['name'];
 		$recipient = $toMailId;
 		$sellername = $customer->getName();
 		$contactno = $seller_info['contact'];
 		$emailTemplate->setSenderName($sellername);
 		$emailTemplate->setSenderEmail($selleremail);
 		$emailTemplateVariables = (array('ownername' => $toName,'selleremail'=>$selleremail,'sellername' => $sellername,'orderId'=>$order_id, 'selleremail' => $selleremail,'productName'=>$productName, 'message' => $message));
 		$emailTemplate->setDesignConfig(array('area' => 'frontend'));
 		$emailTemplate->getProcessedTemplate($emailTemplateVariables);
 		$emailTemplate->send($recipient, $sellername, $emailTemplateVariables);
 		$emailTemplate->send($selleremail, $sellername, $emailTemplateVariables);
 		Mage::getSingleton('core/session')->addSuccess($this->__('Mail has been sent to Admin'));
 		$this->_redirect('sellbuy/order/manage');
 	}  catch (Mage_Core_Exception $e) {
 		/**
 		 *  Error message redirect to order manage page
 		 */
 		Mage::getSingleton('core/session')->addError($this->__($e->getMessage()));
 		$this->_redirect('*/*/manage');
 	}
 }

} 