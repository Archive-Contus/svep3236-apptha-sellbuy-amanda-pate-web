<?php
/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Sellbuy
 * @version     0.1.0
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 * 
 */
/**
 * Manage Seller actions in admin section
 * This Class has been used to manage seller actions in admin section like
 * Save
 * Delete
 * Approval
 * Disapproval
 */
class Apptha_Sellbuy_Adminhtml_ManagesellerController extends Mage_Adminhtml_Controller_action {
    
    protected function _initAction() {
        $this->loadLayout ()->_setActiveMenu ( 'sellbuy/items' )->_addBreadcrumb ( Mage::helper ( 'adminhtml' )->__ ( 'Items Manager' ), Mage::helper ( 'adminhtml' )->__ ( 'Seller Manager' ) );
        return $this;
    }
    /**
     * Load phtml file layout
     *
     * @return void
     */
    public function indexAction() {
        $this->_initAction ()->renderLayout ();
    }
    /**
     * Edit seller data
     *
     * @return void
     */
    public function editAction() {
        $id = $this->getRequest ()->getParam ( 'id' );
        $model = Mage::getModel ( 'sellbuy/sellbuy' )->load ( $id );
        /**
         * Check the model has been set
         * or the posted id has been set
         */
        if ($model->getId () || $id == 0) {
            $data = Mage::getSingleton ( 'adminhtml/session' )->getFormData ( true );
            /**
             * Check the retrieved data is not equal to empty
             */
            if (! empty ( $data )) {
                $model->setData ( $data );
            }
            Mage::register ( 'sellbuy_data', $model );
            /**
             * Initialising layout, breadcumbs
             */
            $this->loadLayout ();
            $this->_setActiveMenu ( 'sellbuy/items' );
            $this->_addBreadcrumb ( Mage::helper ( 'adminhtml' )->__ ( 'Item Manager' ), Mage::helper ( 'adminhtml' )->__ ( 'Seller Manager' ) );
            $this->_addBreadcrumb ( Mage::helper ( 'adminhtml' )->__ ( 'Item News' ), Mage::helper ( 'adminhtml' )->__ ( 'Seller News' ) );
            $this->getLayout ()->getBlock ( 'head' )->setCanLoadExtJs ( true );
            $this->_addContent ( $this->getLayout ()->createBlock ( 'sellbuy/adminhtml_sellbuy_edit' ) )->_addLeft ( $this->getLayout ()->createBlock ( 'sellbuy/adminhtml_sellbuy_edit_tabs' ) );
            $this->renderLayout ();
        } else {
            /**
             * If the retrieved data is equal to empty
             * display the error message
             */
            Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'sellbuy' )->__ ( 'Seller details does not exist' ) );
            $this->_redirect ( '*/*/' );
        }
    }
    /**
     * Edit seller data
     *
     * @return void
     */
    public function newAction() {
        $this->_forward ( 'edit' );
    }
    /**
     * Save seller data
     * and change the status
     *
     * @return void
     */
    public function saveAction() {
        $data = $this->getRequest ()->getPost ();
        if ($data) {
            $model = Mage::getModel ( 'sellbuy/sellbuy' );
            $model->setData ( $data )->setId ( $this->getRequest ()->getParam ( 'id' ) );
            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime () == NULL) {
                    $model->setCreatedTime ( now () )->setUpdateTime ( now () );
                } else {
                    $model->setUpdateTime ( now () );
                }
                $model->save ();
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'sellbuy' )->__ ( 'Please select at least one seller Approved Successfully' ) );
                Mage::getSingleton ( 'adminhtml/session' )->setFormData ( false );
                if ($this->getRequest ()->getParam ( 'back' )) {
                    $this->_redirect ( '*/*/edit', array (
                            'id' => $model->getId () 
                    ) );
                    return;
                }
                $this->_redirect ( '*/*/' );
                return;
            } catch ( Exception $e ) {
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                Mage::getSingleton ( 'adminhtml/session' )->setFormData ( $data );
                $this->_redirect ( '*/*/edit', array (
                        'id' => $this->getRequest ()->getParam ( 'id' ) 
                ) );
                return;
            }
        }
        Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'sellbuy' )->__ ( 'Seller details not updated' ) );
        $this->_redirect ( '*/*/' );
    }
    /**
     * Delete multiple seller's at a time
     *
     * @return void
     */
    public function massDeleteAction() {
        /**
         * Get the sellbuy ids
         */
        $sellbuyIds = $this->getRequest ()->getParam ( 'sellbuy' );
        /**
         * Check the sellbuy ids is not an array
         * if so display error message to selecte atleast one seller
         */
        if (! is_array ( $sellbuyIds )) {
            Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select at least one seller' ) );
        } else {
            try {
                foreach ( $sellbuyIds as $sellbuyId ) {
                    Mage::helper ( 'sellbuy/common' )->deleteSeller ( $sellbuyId );
                    $productCollections = Mage::getModel ( 'catalog/product' )->getCollection ()->addAttributeToFilter ( 'seller_id', $sellbuyId );
                    
                    Mage::helper ( 'sellbuy/general' )->deleteProducts ( $productCollections );
                    Mage::getModel ( 'sellbuy/sellerprofile' )->load ($sellbuyId, 'seller_id' )->delete();
                
                }
                /**
                 * Display Success message upon Deletion
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully deleted', count ( $sellbuyIds ) ) );
            } catch ( Exception $e ) {
                /**
                 * Display Error message
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
            }
        }
        $this->_redirect ( '*/*/index' );
    }
    /**
     * Setting commission for admin
     *
     * @return void
     */
    public function setcommissionAction() {
        $this->_initAction ()->renderLayout ();
    }
    /**
     * Save commission information in database
     *
     * @return void
     */
    public function savecommissionAction() {
        /**
         * Check the posted id is greater than zero
         * if so get the id from posted value
         * and get the commission details from posted commission
         */
        if ($this->getRequest ()->getParam ( 'id' ) > 0) {
            $id = $this->getRequest ()->getParam ( 'id' );
            $commission = $this->getRequest ()->getParam ( 'commission' );
            try {
                $collection = Mage::getModel ( 'sellbuy/sellerprofile' )->load ( $id, 'seller_id' );
                $getId = $collection->getId ();
                /**
                 * Check the retrieved is is not equal to empty
                 * if so Save the commission information
                 */
                if ($getId != '') {
                    Mage::getModel ( 'sellbuy/sellerprofile' )->load ( $id, 'seller_id' )->setCommission ( $commission )->save ();
                } else {
                    $collection = Mage::getModel ( 'sellbuy/sellerprofile' );
                    $collection->setCommission ( $commission );
                    $collection->setSellerId ( $id );
                    $collection->save ();
                }
                /**
                 * Display success message on successful commission save action
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'sellbuy' )->__ ( 'Seller commission saved successfully .' ) );
                $this->_redirect ( '*/*/' );
            } catch ( Exception $e ) {
                /**
                 * Display error message on commission save failure
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                $this->_redirect ( '*/*/' );
            }
        }
        $this->_redirect ( '*/*/' );
    }
    /**
     * Set seller status as approve after seller register in the website
     *
     * @return void
     */
    public function approveAction() {
        /**
         * Confirming the posted id is greater than zero
         * if so get the id from the posted value
         */
        if ($this->getRequest ()->getParam ( 'id' ) > 0) {
            $id = $this->getRequest ()->getParam ( 'id' );
            try {
                $model = Mage::getModel ( 'customer/customer' )->load ( $this->getRequest ()->getParam ( 'id' ) );
                $model->setCustomerstatus ( '1' )->save ();
                /**
                 * send email to customer regarding approval of seller registration
                 */
                $template_id = ( int ) Mage::getStoreConfig ( 'sellbuy/admin_approval_seller_registration/seller_email_template_selection' );
                /**
                 * Get the admin email id
                 */
                $adminEmailIdVal = Mage::getStoreConfig ( 'sellbuy/sellbuy/admin_email_id' );
                /**
                 * Get the to mail id
                 */
                $toMailId = Mage::getStoreConfig ( "trans_email/ident_$adminEmailIdVal/email" );
                /**
                 * Get the to mail id
                 */
                $toName = Mage::getStoreConfig ( "trans_email/ident_$adminEmailIdVal/name" );
                if ($template_id) {
                    $emailTemplateForSeller = Mage::getModel ( 'core/email_template' )->load ( $template_id );
                } else {
                    $emailTemplateForSeller = Mage::getModel ( 'core/email_template' )->loadDefault ( 'sellbuy_admin_approval_seller_registration_seller_email_template_selection' );
                }
                /**
                 * Get the customer information like
                 * customer data
                 * customer email
                 * customer name
                 */
                $customer = Mage::getModel ( 'customer/customer' )->load ( $id );
                $recipient = $customer->getEmail ();
                $cname = $customer->getName ();
                $emailTemplateForSeller->setSenderEmail ( $toMailId );
                $emailTemplateForSeller->setSenderName ( ucwords ( $toName ) );
                /**
                 * Replacing values in email template with dynamic values
                 */
                $emailTemplateForSellerVariables = (array (
                        'cname' => ucwords ( $cname ),
                        'ownername' => ucwords ( $toName ) 
                ));
                $emailTemplateForSeller->setDesignConfig ( array (
                        'area' => 'frontend' 
                ) );
                $emailTemplateForSeller->getProcessedTemplate ( $emailTemplateForSellerVariables );
                /**
                 * Sending email
                 */
                $emailTemplateForSeller->send ( $recipient, ucwords ( $cname ), $emailTemplateForSellerVariables );
                /**
                 * end email
                 */
                $noticMsg = 'Seller approved successfully.';
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'sellbuy' )->__ ( $noticMsg ) );
                $this->_redirect ( '*/*/' );
            } catch ( Exception $e ) {
                /**
                 * Display Error message
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                $this->_redirect ( '*/*/' );
            }
        }
        $this->_redirect ( '*/*/' );
    }
    /**
     * Set seller status as disapprove after seller register in the website
     *
     * @return void
     */
    public function disapproveAction() {
        /**
         * Checking the posted id values is greater than 0
         * if so retreive the posted id value
         */
        if ($this->getRequest ()->getParam ( 'id' ) > 0) {
            $id = $this->getRequest ()->getParam ( 'id' );
            try {
                $model = Mage::getModel ( 'customer/customer' )->load ( $this->getRequest ()->getParam ( 'id' ) );
                $model->setCustomerstatus ( '2' )->save ();
                /**
                 * send email to admin regarding disapprove of seller registration
                 */
                $templateId = ( int ) Mage::getStoreConfig ( 'sellbuy/admin_approval_seller_registration/seller_email_template_disapprove' );
                /**
                 * Get the admin configuration information related mail like
                 * admin email id
                 * to mail id
                 * to name
                 */
                $adminEmailIdData = Mage::getStoreConfig ( 'sellbuy/sellbuy/admin_email_id' );
                $toMailId = Mage::getStoreConfig ( "trans_email/ident_$adminEmailIdData/email" );
                $toName = Mage::getStoreConfig ( "trans_email/ident_$adminEmailIdData/name" );
                /**
                 * Check the template id has been set in admin section
                 * if so load the template id
                 * else load the admin approval seller registration seller email template disapprove template
                 */
                if ($templateId) {
                    $emailTemplateForSeller = Mage::getModel ( 'core/email_template' )->load ( $templateId );
                } else {
                    $emailTemplateForSeller = Mage::getModel ( 'core/email_template' )->loadDefault ( 'sellbuy_admin_approval_seller_registration_seller_email_template_disapprove' );
                }
                /**
                 * get the customer information like
                 * Customer data
                 * customer name
                 * customer email
                 */
                $customer = Mage::getModel ( 'customer/customer' )->load ( $id );
                $recipient = $customer->getEmail ();
                $cname = $customer->getName ();
                /**
                 * Replacing the retrieved values dynamicall in the email template
                 */
                $emailTemplateForSellerVariables = (array (
                        'ownername' => ucwords ( $toName ),
                        'cname' => ucwords ( $cname ) 
                ));
                
                $emailTemplateForSeller->setSenderName ( ucwords ( $toName ) );
                $emailTemplateForSeller->setSenderEmail ( $toMailId );
                
                $emailTemplateForSeller->setDesignConfig ( array (
                        'area' => 'frontend' 
                ) );
                $emailTemplateForSeller->getProcessedTemplate ( $emailTemplateForSellerVariables );
                /**
                 * Send email function
                 */
                $emailTemplateForSeller->send ( $recipient, ucwords ( $cname ), $emailTemplateForSellerVariables );
                /**
                 * end email
                 * Display the success message on seller disapprove success
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'sellbuy' )->__ ( 'Seller disapproved.' ) );
                $this->_redirect ( '*/*/' );
            } catch ( Exception $e ) {
                /**
                 * Display error message if the seller action is failure
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                $this->_redirect ( '*/*/' );
            }
        }
        $this->_redirect ( '*/*/' );
    }
    /**
     * Dellete seller from website
     *
     * @return void
     */
    public function deleteAction() {
        if ($this->getRequest ()->getParam ( 'id' ) > 0) {
            try {
                /**
                 * Reset group id
                 */
                $model = Mage::getModel ( 'customer/customer' )->load ( $this->getRequest ()->getParam ( 'id' ) );
                $model->setGroupId ( 1 );
                $model->save ();
                $productCollections = Mage::getModel ( 'catalog/product' )->getCollection ()->addAttributeToFilter ( 'seller_id', $this->getRequest ()->getParam ( 'id' ) );
                foreach ( $productCollections as $product ) {
                    $productId = $product->getEntityId ();
                    $model = Mage::getModel ( 'catalog/product' )->load ( $productId );
                    $model->delete ();
                }
                
                Mage::getModel ( 'sellbuy/sellerprofile' )->load ( $this->getRequest ()->getParam ( 'id' ), 'seller_id' )->delete();
                /**
                 * show success message on successfull seller deletion
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Seller successfully deleted' ) );
                $this->_redirect ( '*/*/' );
            } catch ( Exception $e ) {
                /**
                 * Error message on seller deletion failure
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                $this->_redirect ( '*/*/edit', array (
                        'id' => $this->getRequest ()->getParam ( 'id' ) 
                ) );
            }
        }
        $this->_redirect ( '*/*/' );
    }
    /**
     * Set seller status as approve multiple seller register in the website
     *
     * @return void
     */
    public function massApproveAction() {
        /**
         * Retrieve the sell buy ids from posted values
         */
        $sellbuyIds = $this->getRequest ()->getParam ( 'sellbuy' );
        /**
         * Check the sell buy ids is not an array
         * if so display error message like please select atleast one seller
         */
        if (! is_array ( $sellbuyIds )) {
            Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select at least one seller' ) );
        } else {
            try {
                foreach ( $sellbuyIds as $sellbuyId ) {
                    Mage::helper ( 'sellbuy/common' )->approveSellerStatus ( $sellbuyId );
                    /**
                     * send email to customer regarding approval of seller registration
                     */
                    $template_id = ( int ) Mage::getStoreConfig ( 'sellbuy/admin_approval_seller_registration/seller_email_template_selection' );
                    /**
                     * Get the stored configured information like
                     * Admin email id
                     * to mail id
                     * to name
                     */
                    $adminEmailIdValue = Mage::getStoreConfig ( 'sellbuy/sellbuy/admin_email_id' );
                    $toMailId = Mage::getStoreConfig ( "trans_email/ident_$adminEmailIdValue/email" );
                    $toName = Mage::getStoreConfig ( "trans_email/ident_$adminEmailIdValue/name" );
                    $emailTemplateForSeller = Mage::helper ( 'sellbuy/general' )->getSellerApprovalEmailTemplate ( $template_id, 1 );
                    /**
                     * Get he information of customer like
                     * name of the customer
                     * email id of the customer
                     */
                    $customer = Mage::helper ( 'sellbuy/sellbuy' )->loadCustomerData ( $sellbuyId );
                    $recipient = $customer->getEmail ();
                    $cname = $customer->getName ();
                    $emailTemplateForSeller->setSenderName ( ucwords ( $toName ) );
                    $emailTemplateForSeller->setSenderEmail ( $toMailId );
                    /**
                     * Replace the mail template variables with the retrieved information dynamically
                     */
                    $emailTemplateForSellerVariables = (array (
                            'ownername' => ucwords ( $toName ),
                            'cname' => ucwords ( $cname ) 
                    ));
                    $emailTemplateForSeller->setDesignConfig ( array (
                            'area' => 'frontend' 
                    ) );
                    $emailTemplateForSeller->getProcessedTemplate ( $emailTemplateForSellerVariables );
                    $emailTemplateForSeller->send ( $recipient, ucwords ( $cname ), $emailTemplateForSellerVariables );
                /**
                 * end email
                 */
                }
                /**
                 * Display succcess message after successfull approval
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'A total of %d record(s) is successfully approved', count ( $sellbuyIds ) ) );
            } catch ( Exception $e ) {
                /**
                 * Display error message if the approval has been failed
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
            }
        }
        $this->_redirect ( '*/*/index' );
    }
    /**
     * Set seller status as disapprove multiple seller register in the website
     *
     * @return void
     */
    public function massDisapproveAction() {
        /**
         * Get all the seel buy ids from posted values
         */
        $sellbuyIds = $this->getRequest ()->getParam ( 'sellbuy' );
        /**
         * Check the sell buy ids is not an array
         * if so add error message like select seller
         */
        if (! is_array ( $sellbuyIds )) {
            Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select at least one seller' ) );
        } else {
            try {
                foreach ( $sellbuyIds as $sellbuyId ) {
                    Mage::helper ( 'sellbuy/common' )->disapproveSellerStatus ( $sellbuyId );
                    /**
                     * send email to admin regarding disapprove of seller registration
                     */
                    $template_id = ( int ) Mage::getStoreConfig ( 'sellbuy/admin_approval_seller_registration/seller_email_template_disapprove' );
                    /**
                     * Get and assign the values like admin email id, to mail id, to name from admin store configuration
                     * if so display error message to selecte atleast one seller
                     */
                    $adminEmailId = Mage::getStoreConfig ( 'sellbuy/sellbuy/admin_email_id' );
                    $toMailId = Mage::getStoreConfig ( "trans_email/ident_$adminEmailId/email" );
                    $toName = Mage::getStoreConfig ( "trans_email/ident_$adminEmailId/name" );
                    
                    $emailTemplateForSeller = Mage::helper ( 'sellbuy/general' )->getSellerApprovalEmailTemplate ( $template_id, 2 );
                    /**
                     * Retrieve the customer info like
                     * customername
                     * customer mail id
                     */
                    $customer = Mage::helper ( 'sellbuy/sellbuy' )->loadCustomerData ( $sellbuyId );
                    $cname = $customer->getName ();
                    $recipient = $customer->getEmail ();
                    
                    /**
                     * Dyanamically replace the retrieved values in email template
                     */
                    $emailTemplateForSeller->setSenderName ( ucwords ( $toName ) );
                    $emailTemplateForSeller->setSenderEmail ( $toMailId );
                    
                    $emailTemplateForSeller->setDesignConfig ( array (
                            'area' => 'frontend' 
                    ) );
                    $emailTemplateSellerVariables = (array (
                            'ownername' => ucwords ( $toName ),
                            'cname' => ucwords ( $cname ) 
                    ));
                    $emailTemplateForSeller->getProcessedTemplate ( $emailTemplateSellerVariables );
                    /**
                     * Email sending function for the dynamically replaced values
                     */
                    $emailTemplateForSeller->send ( $recipient, ucwords ( $cname ), $emailTemplateSellerVariables );
                /**
                 * end email
                 */
                }
                /**
                 * Success message of records on disapproval
                 */
                $successMsg = Mage::helper ( 'adminhtml' )->__ ( 'A total of %d record(s) is successfully disapproved', count ( $sellbuyIds ) );
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( $successMsg );
            } catch ( Exception $e ) {
                /**
                 * Error message of records on disapproval failure
                 */
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
            }
        }
        $this->_redirect ( '*/*/index' );
    }
} 