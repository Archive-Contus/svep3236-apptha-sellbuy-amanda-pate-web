<?php

/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Sellbuy
 * @version     0.1.0
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 */

/**
 * Manage Sellers Grid
 */
class Apptha_Sellbuy_Block_Adminhtml_Manageseller_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    
    /**
     * Construct the inital display of grid information
     * Set the default sort for collection
     * Set the sort order as "DESC"
     *
     * Return array of data to display seller information
     *
     * @return array
     */
    public function __construct() {
        parent::__construct ();
        $this->setId ( 'managesellerGrid' );
        $this->setDefaultSort ( 'entity_id' );
        $this->setDefaultDir ( 'DESC' );
        $this->setSaveParametersInSession ( true );
    }
    
    /**
     * Function to get seller collection
     *
     * Return the seller information
     * return array
     */
    protected function _prepareCollection() {
        $gid = Mage::helper ( 'sellbuy' )->getGroupId ();
        $collection = Mage::getResourceModel ( 'customer/customer_collection' )->addNameToSelect ()->addAttributeToSelect ( 'email' )->addAttributeToSelect ( 'created_at' )->addAttributeToSelect ( 'group_id' )->addAttributeToSelect ( 'customerstatus' )->addFieldToFilter ( 'group_id', $gid );
        $this->setCollection ( $collection );
        return parent::_prepareCollection ();
    }
    
    /**
     * Function to create column to grid
     *
     * @param string $id            
     * @return string colunm value
     */
    public function addCustomColumn($id) {
        switch ($id) {
            case 'ID' :
            $entityId = array ('header' => Mage::helper ( 'customer' )->__ ( 'ID' ),'width' => '40px','index' => 'entity_id');
                $value = $this->addColumn ( 'entity_id',$entityId);
                break;
            case 'Store Name' :
            $storeTitle = array ('header' => Mage::helper ( 'customer' )->__ ( 'Store Name' ),'width' => '150px','index' => 'store_title',
                'filter' => false,'renderer' => 'Apptha_Sellbuy_Block_Adminhtml_Renderersource_Storetitle');
                $value = $this->addColumn ( 'store_title',$storeTitle);
                break;
            case 'email' :
            $email = array ('header' => Mage::helper ( 'customer' )->__ ( 'email' ),'width' => '160px','index' => 'email');
                $value = $this->addColumn ( 'email',$email);
                break;
            case 'Name' :
            $name = array ('header' => Mage::helper ( 'customer' )->__ ( 'Name' ),'width' => '200px','index' => 'name');
                $value = $this->addColumn ( 'name',$name);
                break;
            case 'Contact' :
            $contact = array ('header' => Mage::helper ( 'customer' )->__ ( 'Contact' ),'width' => '150px','index' => 'contact',
                'filter' => false,'renderer' => 'Apptha_Sellbuy_Block_Adminhtml_Renderersource_Contact');
                $value = $this->addColumn ( 'contact',$contact);
                break;
            case 'customer' :
                $customerSince = array ('header' => Mage::helper ( 'customer' )->__ ( 'customer' ),'type' => 'datetime','width' => '150px',
                'align' => 'center','index' => 'created_at','gmtoffset' => true);
            $value = $this->addColumn ( 'customer_since',$customerSince);
                break;
            case 'Total Sales' :
            $totalSales = array ('header' => Mage::helper ( 'customer' )->__ ( 'Total Sales' ),'width' => '80px','index' => 'total_sales',
                'align' => 'right','filter' => false,'renderer' => 'Apptha_Sellbuy_Block_Adminhtml_Renderersource_Totalsales');
                $value = $this->addColumn ( 'total_sales',$totalSales);
                break;
            case 'Received' :
            $received = array ('header' => Mage::helper ( 'sales' )->__ ( 'Received' ),'width' => '80px','align' => 'right','index' => 'entity_id',
                        'filter' => false,'renderer' => 'Apptha_Sellbuy_Block_Adminhtml_Renderersource_Amountreceived');
                $value = $this->addColumn ( 'amount_received',$received);
                break;
            case 'Remaining' :
                $remaining = array ('header' => Mage::helper ( 'sales' )->__ ( 'Remaining' ),'width' => '80px','align' => 'right','index' => 'entity_id',
                'filter' => false,'renderer' => 'Apptha_Sellbuy_Block_Adminhtml_Renderersource_Amountremaining');
            $value = $this->addColumn ( 'amount_remaining',$remaining);
                break;
            default :
            $customerstatus = array ('header' => Mage::helper ( 'customer' )->__ ( 'Status' ),'width' => '150px','type' => 'options','index' => 'customerstatus',
            'options' => Mage::getSingleton ( 'sellbuy/status_status' )->getOptionArray ());
                $value = $this->addColumn ( 'customerstatus',$customerstatus);
        }
        return $value;
    }
    
    /**
     * Function to display fields with data
     *
     * Display information about Seller
     *
     * @return void
     */
    protected function _prepareColumns() {
        $this->addCustomColumn ( 'ID' );
        $this->addCustomColumn ( 'Store Name' );
        $this->addCustomColumn ( 'email' );
        $this->addCustomColumn ( 'Name' );
        $this->addCustomColumn ( 'Contact' );
        $this->addCustomColumn ( 'customer' );
        $this->addColumn ( 'total_products', array (
                'header' => Mage::helper ( 'customer' )->__ ( '#Products' ),
                'width' => '100px',
                'index' => 'total_products',
                'align' => 'right',
                'filter' => false,
                'renderer' => 'Apptha_Sellbuy_Block_Adminhtml_Renderersource_Totalproducts' 
        ) );
        $this->addColumn ( 'commission', array (
                'header' => Mage::helper ( 'customer' )->__ ( 'Commission(%)' ),
                'width' => '70px',
                'index' => 'commission',
                'align' => 'right',
                'filter' => false,
                'renderer' => 'Apptha_Sellbuy_Block_Adminhtml_Renderersource_Commissionrate' 
        ) );
        $this->addCustomColumn ( 'Total Sales' );
        $this->addCustomColumn ( 'Received' );
        $this->addCustomColumn ( 'Remaining' );
        $this->addCustomColumn ( 'Status' );
        $this->addColumn ( 'action', array (
                'header' => Mage::helper ( 'sellbuy' )->__ ( 'action' ),
                'type' => 'action',
                'width' => '200px',
                'getter' => 'getId',
                'actions' => array (
                        array (
                                'caption' => Mage::helper ( 'sellbuy' )->__ ( 'Approve' ),
                                'url' => array (
                                        'base' => '*/*/approve/' 
                                ),
                                'field' => 'id',
                                'title' => Mage::helper ( 'sellbuy' )->__ ( 'Approve' ) 
                        ),
                        array (
                                'caption' => Mage::helper ( 'sellbuy' )->__ ( 'Disapprove' ),
                                'url' => array (
                                        'base' => "*/*/disapprove" 
                                ),
                                'field' => 'id' 
                        ),
                        array (
                                'caption' => Mage::helper ( 'sellbuy' )->__ ( 'Delete' ),
                                'url' => array (
                                        'base' => "*/*/delete" 
                                ),
                                'field' => 'id',
                                'confirm' => Mage::helper ( 'sellbuy' )->__ ( 'Are you sure?' ) 
                        ) 
                ),
                'sortable' => false 
        ) );
        /**
         * set commission
         */
        $this->addColumn ( 'set_commission', array (
                'header' => Mage::helper ( 'sellbuy' )->__ ( 'Set Commission' ),
                'width' => '80',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array (
                        array (
                                'caption' => Mage::helper ( 'sellbuy' )->__ ( 'Commission' ),
                                'url' => array (
                                        'base' => '*/*/setcommission/' 
                                ),
                                'field' => 'id',
                                'title' => Mage::helper ( 'sellbuy' )->__ ( 'Commission' ) 
                        ) 
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true 
        ) );
        /**
         * Edit Action
         */
        $order = array ('header' => Mage::helper ( 'sellbuy' )->__ ( 'Order' ),'width' => '80','type' => 'action','getter' => 'getId',
                'actions' => array (array ('caption' => Mage::helper ( 'sellbuy' )->__ ( 'Order' ),'url' => array ('base' => 'sellbuyadmin/adminhtml_order/index/'),
                'field' => 'id')),'filter' => false,'sortable' => false,'index' => 'stores','is_system' => true);
        $this->addColumn ( 'order',$order);
        return parent::_prepareColumns ();
    }
    
    /**
     * Function for Mass edit action(approve,disapprove or delete)
     *
     * Will change the status of the seller
     * return void
     */
    protected function _prepareMassaction() {
        $this->setMassactionIdField ( 'entity_id' );
        $this->getMassactionBlock ()->setFormFieldName ( 'sellbuy' );
        $this->getMassactionBlock ()->addItem ( 'delete', array (
                'label' => Mage::helper ( 'sellbuy' )->__ ( 'Delete' ),
                'url' => $this->getUrl ( '*/*/massDelete' ),
                'confirm' => Mage::helper ( 'sellbuy' )->__ ( 'Are you sure?' ) 
        ) );
        $this->getMassactionBlock ()->addItem ( 'Approve', array (
                'label' => Mage::helper ( 'customer' )->__ ( 'Approve' ),
                'url' => $this->getUrl ( '*/*/massApprove' ) 
        ) );
        $this->getMassactionBlock ()->addItem ( 'disapprove', array (
                'label' => Mage::helper ( 'customer' )->__ ( 'Disapprove' ),
                'url' => $this->getUrl ( '*/*/massDisapprove' ) 
        ) );
        return $this;
    }
    
    /**
     * Function for link url
     *
     * Return the seller profile edit url
     * return string
     */
    public function getRowUrl($row) {
        return $this->getUrl ( 'adminhtml/customer/edit/', array (
                'id' => $row->getId () 
        ) );
    }
}

