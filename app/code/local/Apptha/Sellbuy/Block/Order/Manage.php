<?php

/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Apptha
 * @package     Apptha_Sellbuy
 * @version     0.1.0
 * @author      Apptha Team <developers@contus.in>
 * @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
 * @license     http://www.apptha.com/LICENSE.txt
 * 
 */

/**
 * Manage order information
 * Manage order information with seller details and also with pagination
 */
class Apptha_Sellbuy_Block_Order_Manage extends Mage_Core_Block_Template {
    
    /**
     * Collection for manage orders
     *
     * @return \Apptha_Sellbuy_Block_Order_Manage
     */
    protected function _prepareLayout() {
        parent::_prepareLayout ();
        $manageCollection = $this->getsellerOrders ();
        $this->setCollection ( $manageCollection );
        $pager = $this->getLayout ()->createBlock ( 'page/html_pager', 'my.pager' )->setCollection ( $manageCollection );
        $pager->setAvailableLimit ( array (
        		5  => 5,
                10 => 10,
                20 => 20,
                50 => 50 
        ) );
        $this->setChild ( 'pager', $pager );
        return $this;
    }
    
    /**
     * Function to get pagination
     *
     * Return pagination for collection
     *
     * @return array
     */
    public function getPagerHtml() {
        return $this->getChildHtml ( 'pager' );
    }
    
    /**
     * Function to get seller order details
     *
     * Return seller orders information
     *
     * @return array
     */
    public function getsellerOrders() {
        /**
         * Initialising empty values with variables like
         * var data,status,select,filter,from,to
         */
        $data = $status = $selectFilter = $from = $to = '';
        $lastWeekDateVal = array ();
        /**
         * Get the posted values in variable
         */
        $data = $this->getRequest ()->getPost ();
        if (isset ( $data ['status'] )) {
            $status = $data ['status'];
        }
        /**
         * Check the select filter has been set
         * if so assign the value in a variable
         */
        if (isset ( $data ['select_filter'] )) {
            $selectFilter = $data ['select_filter'];
        }
        $orders = Mage::getModel ( 'sellbuy/commission' )->getCollection ()->addFieldToSelect ( '*' )->addFieldToFilter ( 'seller_id', Mage::getSingleton ( 'customer/session' )->getCustomer ()->getId () );
        /**
         * Check the Select filter is not equal to empty
         */
        if (! empty ( $selectFilter )) {
            switch ($selectFilter) {
                case "today" :
                    $startDay = strtotime ( "-1 today midnight" );
                    $endDay = strtotime ( "-1 tomorrow midnight" );
                    $from = date ( 'Y-m-d', $startDay );
                    $to = date ( 'Y-m-d', $endDay );
                    $fromDisplayValue = date ( 'Y-m-d', $startDay );
                    $toDisplayValue = date ( 'Y-m-d', $startDay );
                    break;
                case "yesterday" :
                    $startDay = strtotime ( "-1 yesterday midnight" );
                    $endDay = strtotime ( "-1 today midnight" );
                    $from = date ( 'Y-m-d', $startDay );
                    $to = date ( 'Y-m-d', $endDay );
                    $fromDisplayValue = date ( 'Y-m-d', $startDay );
                    $toDisplayValue = date ( 'Y-m-d', $startDay );
                    break;
                case "lastweek" :
                    $lastWeekDateVal = $this->getLastWeekDate ();
                    $startDay = $lastWeekDateVal [0];
                    $endDay = $lastWeekDateVal [1];
                    $from = date ( 'Y-m-d', $startDay );
                    $to = date ( 'Y-m-d', $endDay );
                    $to = date ( 'Y-m-d', strtotime ( $to . ' + 1 day' ) );
                    $fromDisplayValue = $from;
                    $toDisplayValue = date ( 'Y-m-d', $endDay );
                    break;
                case "lastmonth" :
                    /**
                     * last month interval
                     */
                    $from = date ( 'Y-m-01', strtotime ( 'last month' ) );
                    $to = date ( 'Y-m-t', strtotime ( 'last month' ) );
                    $to = date ( 'Y-m-d', strtotime ( $to . ' + 1 day' ) );
                    $fromDisplayValue = $from;
                    $toDisplayValue = date ( 'Y-m-t', strtotime ( 'last month' ) );
                    break;
                case "custom" :
                    /**
                     * last custom interval
                     */
                    $from = date ( 'Y-m-d', strtotime ( $data ['date_from'] ) );
                    $to = date ( 'Y-m-d', strtotime ( $data ['date_to'] . ' + 1 day' ) );
                    $fromDisplayValue = $from;
                    $toDisplayValue = date ( 'Y-m-d', strtotime ( $data ['date_to'] ) );
                    break;
                default :
            }
            /**
             * Convert local date to magento db date.
             */
            $dbFrom = Mage::getModel ( 'core/date' )->gmtDate ( null, strtotime ( $from ) );
            $dbTo = Mage::getModel ( 'core/date' )->gmtDate ( null, strtotime ( $to ) );
            $orders->addFieldToFilter ( 'created_at', array (
                    'from' => $dbFrom,
                    'to' => $dbTo 
            ) );
        }
        /**
         * Check the status is not equal to empty value
         * if so update the order status in
         */
        if ($status != '') {
            $orders->addFieldToFilter ( 'order_status', array (
                    'in' => array (
                            $status 
                    ) 
            ) );
        }
        $orders->setOrder ( 'order_id', 'desc' );
        return $orders;
    }
    public function getLastWeekDate() {
        $lastWeekDateVal = array ();
        $to = date ( 'd-m-Y' );
        $toDay = date ( 'l', strtotime ( $to ) );
        /**
         * Calculate the stat day and end day depends upon the to day
         */
        if ($toDay == 'Monday') {
            $startDay = strtotime ( "-1 monday midnight" );
            $endDay = strtotime ( "yesterday" );
        } else {
            $startDay = strtotime ( "-2 monday midnight" );
            $endDay = strtotime ( "-1 sunday midnight" );
        }
        $lastWeekDateVal [] = $startDay;
        $lastWeekDateVal [] = $endDay;
        
        return $lastWeekDateVal;
    }
    /**
     * Function to get Order Status
     *
     * @param String $name
     * @param Int $id
     * @return String
     */
    public function getOrderStatus($name,$id){
    	 
    	$status='';
    	$orders = Mage::getModel('sellbuy/updatestatus')->getCollection();
    	$orders->addFieldToFilter('product_name',array('eq'=>$name)) ;
    	$orders->addFieldToFilter('order_id',array('eq'=>$id));
    	$orders=$orders->getData();
    	foreach ($orders as $order){
    		$status=$order['status'];
    	}
    	return $status;
    }
    /**
     * Function to get Invoice quantity
     * @param Int $getOrderId
     * @param Int $sku
     * @param Int $qty
     * @return number
     */
    public function getInvoice($getOrderId,$sku,$qty){
    	$orders = Mage::getModel('sales/order_item')->getCollection();
    	$orders->addFieldToFilter('order_id',array('eq'=>$getOrderId));
    	$orders->addFieldToFilter('sku',array('eq'=>$sku));
    	$orders->getData();
    	foreach ($orders as $order){
    		if(round($order['qty_invoiced'])>=1){
    			$quantity=round($order['qty_invoiced']);
    		}
    	}
    	return $quantity;
    }
    /**
     * Function to get Shippment quantity
     * @param Int $getOrderId
     * @param Int $sku
     * @param Int $qty
     * @return number
     */
    public  function getShipment($getOrderId,$sku,$qty)  {
    	$orders = Mage::getModel('sales/order_item')->getCollection();
    	$orders->addFieldToFilter('order_id',array('eq'=>$getOrderId));
    	$orders->addFieldToFilter('sku',array('eq'=>$sku));
    	$orders->getData();
    	foreach ($orders as $order){
    		if(round($order['qty_shipped'])>=1){
    			$quantity=round($order['qty_shipped']);
    		}
    	}
    	return $quantity;
    }
    /**
     * Function to get Cancelled quantity
     * @param Int $getOrderId
     * @param Int $sku
     * @param Int $qty
     * @return number
     */
    public  function getCancel($getOrderId,$sku,$qty){
    	$orders = Mage::getModel('sales/order_item')->getCollection();
    	$orders->addFieldToFilter('order_id',array('eq'=>$getOrderId));
    	$orders->addFieldToFilter('sku',array('eq'=>$sku));
    	$orders->getData();
    	foreach ($orders as $order){
    		if(round($order['qty_canceled'])>=1){
    			$quantity=round($order['qty_canceled']);
    		}
    	}
    	return $quantity;
    }
}

