<?php

/**
 * Apptha
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.apptha.com/LICENSE.txt
*
* ==============================================================
*                 MAGENTO EDITION USAGE NOTICE
* ==============================================================
* This package designed for Magento COMMUNITY edition
* Apptha does not guarantee correct work of this extension
* on any other Magento edition except Magento COMMUNITY edition.
* Apptha does not provide extension support in case of
* incorrect edition usage.
* ==============================================================
*
* @category    Apptha
* @package     Apptha_Sellbuy
* @version     0.1.0
* @author      Apptha Team <developers@contus.in>
* @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
* @license     http://www.apptha.com/LICENSE.txt
*/
class Apptha_Sellbuy_Block_Order_Grid_Render extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
	
	public function render(Varien_Object $row)
	{
		$model = Mage::getModel('sales/order');
		$product=$model->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)->load($row->getEntityId());
		$product = $product->getAllItems();
		$increment=$row->getData('increment_id');
		$orderStatus=$row->getData('status');
		
		foreach($product as $item){
			$order = Mage::getModel('sellbuy/updatestatus')->getCollection();
			$order->addFieldToFilter('product_name',array('eq'=>$item->getName())) ; 		   		
			$order->addFieldToFilter('order_id',array('eq'=>$increment)); 
			$orders=$order->getData();
			foreach ($orders as $order){
				$status=$order['status'];
				$name=$order['product_name'];
				$_product = Mage::getModel('catalog/product')->loadByAttribute('name',$name);
				$sku=$_product['sku'];}
				
				if($sku==$item->getSku()){
					if(($status=='cancel')){
						echo '<img src="'.$this->getSkinUrl('images/status/close.png').'" style="vertical-align: middle;"/>';
						echo "<span style='vertical-align: middle;'>".$name."</span>";
						echo "<br>";
					} if(($status=='processing')){
						echo '<img src="'.$this->getSkinUrl('images/status/going.png').'" style="vertical-align: middle;" />';
						echo "<span style='vertical-align: middle;'>".$item->getName()."</span>";
						echo "<br>";
					}
				}
				elseif(($item->getQtyShipped() == $item->getQtyOrdered())&&($item->getQtyInvoiced() == $item->getQtyOrdered())){

					echo '<img src="'.$this->getSkinUrl('images/status/right.png').'" style="vertical-align: middle;" />';
					echo "<span style='vertical-align: middle;'>".$item->getName()."</span>";
					echo "<br>";
				}
		
		 		else{
		 			echo '<img src="'.$this->getSkinUrl('images/status/going.png').'" style="vertical-align: middle;" />';
						echo "<span style='vertical-align: middle;'>".$item->getName()."</span>";
					echo "<br>";
				}
			}
		}
	}