<?php
/**
 * Apptha
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.apptha.com/LICENSE.txt
*
* ==============================================================
*                 MAGENTO EDITION USAGE NOTICE
* ==============================================================
* This package designed for Magento COMMUNITY edition
* Apptha does not guarantee correct work of this extension
* on any other Magento edition except Magento COMMUNITY edition.
* Apptha does not provide extension support in case of
* incorrect edition usage.
* ==============================================================
*
* @category    Apptha
* @package     Apptha_Sellbuy
* @version     0.1.0
* @author      Apptha Team <developers@contus.in>
* @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
* @license     http://www.apptha.com/LICENSE.txt
*
*/

/**
* This file is used to create table for "update status"
*/
		$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/*
 * Table structure for table `sellbuy_updatestatus`
 */
$installer->run("
  DROP TABLE IF EXISTS {$this->getTable('sellbuy_updatestatus')};

  CREATE TABLE IF NOT EXISTS {$this->getTable('sellbuy_updatestatus')} (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `status` text CHARACTER SET utf8 NOT NULL,
  `product_name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ");
$installer->endSetup();