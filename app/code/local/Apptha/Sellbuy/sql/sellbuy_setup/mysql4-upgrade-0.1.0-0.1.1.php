<?php
/**
 * Apptha
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.apptha.com/LICENSE.txt
*
* ==============================================================
*                 MAGENTO EDITION USAGE NOTICE
* ==============================================================
* This package designed for Magento COMMUNITY edition
* Apptha does not guarantee correct work of this extension
* on any other Magento edition except Magento COMMUNITY edition.
* Apptha does not provide extension support in case of
* incorrect edition usage.
* ==============================================================
*
* @category    Apptha
* @package     Apptha_Sellbuy
* @version     0.1.0
* @author      Apptha Team <developers@contus.in>
* @copyright   Copyright (c) 2015 Apptha. (http://www.apptha.com)
* @license     http://www.apptha.com/LICENSE.txt
*
*/

/**
* This file is used to create table for "Seller Vacation Mode"
*/
		$installer = $this;

/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
/**
 * create compare_product_id attribute
*/
/**
 * City attribute
 */
$setup->addAttribute('catalog_product', 'city', array(
		'group' => 'Distance Sort',
		'input' => 'text',
		'type' => 'varchar',
		'label' => 'City',
		'backend' => '',
		'frontend' => '',
		'visible' => 1,
		'required' => 0,
		'user_defined' => 1,
		'filterable' => 0,
		'comparable' => 1,
		'visible_on_front' => 1,
		'visible_in_advanced_search' => 1,
		'is_html_allowed_on_front' => 0,
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
));
$setup->updateAttribute('catalog_product', 'city', 'is_searchable',1);
$setup->updateAttribute('catalog_product', 'city', 'used_in_product_listing',1);
$setup->updateAttribute('catalog_product', 'city', 'used_for_sort_by',1);
/**
 * Country attribute
 */
$setup->addAttribute('catalog_product', 'country', array(
		'group' => 'Distance Sort',
		'input' => 'text',
		'type' => 'varchar',
		'label' => 'Country',
		'backend' => '',
		'frontend' => '',
		'visible' => 1,
		'required' => 0,
		'user_defined' => 1,
		'filterable' => 0,
		'comparable' => 1,
		'visible_on_front' => 1,
		'visible_in_advanced_search' => 1,
		'is_html_allowed_on_front' => 0,
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
));
$setup->updateAttribute('catalog_product', 'country', 'is_searchable',1);
$setup->updateAttribute('catalog_product', 'country', 'used_in_product_listing',1);
$setup->updateAttribute('catalog_product', 'country', 'used_for_sort_by',1);

/**
 * Street Address attribute
 */
$setup->addAttribute('catalog_product', 'street_address', array(
		'group' => 'Distance Sort',
		'input' => 'text',
		'type' => 'varchar',
		'label' => 'Street Address',
		'backend' => '',
		'frontend' => '',
		'visible' => 1,
		'required' => 0,
		'user_defined' => 1,
		'filterable' => 0,
		'comparable' => 1,
		'visible_on_front' => 1,
		'visible_in_advanced_search' => 1,
		'is_html_allowed_on_front' => 0,
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
));
$setup->updateAttribute('catalog_product', 'street_address', 'is_searchable',1);
$setup->updateAttribute('catalog_product', 'street_address', 'used_in_product_listing',1);
$setup->updateAttribute('catalog_product', 'street_address', 'used_for_sort_by',1);
/**
 * Postal Code attribute
 */
$setup->addAttribute('catalog_product', 'postal_code', array(
		'group' => 'Distance Sort',
		'input' => 'text',
		'type' => 'varchar',
		'label' => 'Postal Code',
		'backend' => '',
		'frontend' => '',
		'visible' => 1,
		'required' => 0,
		'user_defined' => 1,
		'filterable' => 0,
		'comparable' => 1,
		'visible_on_front' => 1,
		'visible_in_advanced_search' => 1,
		'is_html_allowed_on_front' => 0,
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
));
$setup->updateAttribute('catalog_product', 'postal_code', 'is_searchable',1);
$setup->updateAttribute('catalog_product', 'postal_code', 'used_in_product_listing',1);
$setup->updateAttribute('catalog_product', 'postal_code', 'used_for_sort_by',1);
/**
 * Closing Installer set up
 */
$installer->endSetup ();